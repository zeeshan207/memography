-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 02, 2017 at 05:40 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `director`
--

-- --------------------------------------------------------

--
-- Table structure for table `district`
--

CREATE TABLE `district` (
  `district_id` int(11) NOT NULL,
  `district_name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `district`
--

INSERT INTO `district` (`district_id`, `district_name`) VALUES
(1, 'KASUR'),
(2, 'LAHORE'),
(3, 'SHEIKHUPURA'),
(4, 'NANKANA SAHIB'),
(5, 'BAHAWALNAGAR'),
(6, 'BAHAWALPUR'),
(7, 'RAHIM YAR KHAN'),
(8, 'DERA GHAZI KHAN'),
(9, 'LAYYAH'),
(10, 'MUZAFFAR GARH'),
(11, 'RAJAN PUR'),
(12, 'FAISALABAD'),
(13, 'JHANG'),
(14, 'T.T.SINGH'),
(15, 'CHINIOT'),
(16, 'GUJRANWALA'),
(17, 'GUJRAT'),
(18, 'SIALKOT'),
(19, 'NAROWAL'),
(20, 'HAFIZABAD'),
(21, 'MANDI BAHUDDIN'),
(22, 'MULTAN'),
(23, 'VEHARI'),
(24, 'KHANEWAL'),
(25, 'LODHRAN'),
(26, 'ATTOCK'),
(27, 'CHAKWAL'),
(28, 'JHELUM'),
(29, 'RAWALPINDI'),
(30, 'OKARA'),
(31, 'SAHIWAL'),
(32, 'PAKPATTAN'),
(33, 'BHAKKAR'),
(34, 'KHUSHAB'),
(35, 'MIANWALI'),
(36, 'SARGODHA');

-- --------------------------------------------------------

--
-- Table structure for table `finding_details`
--

CREATE TABLE `finding_details` (
  `fd_id` int(10) NOT NULL,
  `rf_idFK` int(10) NOT NULL,
  `density` enum('Fatty','Mildly Dense','Moderately Dense','Only Dense') NOT NULL,
  `focal_mass` varchar(100) NOT NULL,
  `cm_cal` enum('yes','no') NOT NULL,
  `lbm_cal` enum('yes','no') NOT NULL,
  `a_dis` enum('yes','no') NOT NULL,
  `asymmetry` enum('yes','no') NOT NULL,
  `quadrant` enum('UO','UI','LU','LI','C','LO') NOT NULL,
  `birad` enum('0','1','2','3','4a','4b','4c','5','6') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `image_id` int(10) NOT NULL,
  `rf_idFK` int(10) NOT NULL,
  `image_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`image_id`, `rf_idFK`, `image_name`) VALUES
(1, 366, '14955852_1214787058615366_751427640547380430_n.png');

-- --------------------------------------------------------

--
-- Table structure for table `report_form`
--

CREATE TABLE `report_form` (
  `rf_id` int(10) NOT NULL,
  `token` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `name` varchar(50) NOT NULL,
  `surname` varchar(100) NOT NULL,
  `age` varchar(100) NOT NULL,
  `address` varchar(200) NOT NULL,
  `f_number` varchar(100) NOT NULL,
  `h_number` varchar(100) NOT NULL,
  `district_idFK` int(10) NOT NULL,
  `tehsil_idFK` int(10) NOT NULL,
  `status` enum('single','married') DEFAULT NULL,
  `child` int(10) NOT NULL,
  `height` varchar(100) NOT NULL,
  `weight` int(10) NOT NULL,
  `bmi` int(10) NOT NULL,
  `lmp_date` date NOT NULL,
  `test` enum('screening','diagnostic') DEFAULT NULL,
  `out_come` enum('Normal Mammography','DCIS','LCIS','DCIS + LCIS','ILC','IDC','IBC') NOT NULL,
  `chest` enum('left','right') NOT NULL,
  `sc_date` date DEFAULT NULL,
  `investi` enum('Recall imaging','Call for Biopsy') DEFAULT NULL,
  `doc_file` varchar(55) NOT NULL,
  `mr_no` varchar(200) NOT NULL,
  `post_meno` enum('Yes','No') DEFAULT NULL,
  `pre_meno` enum('Yes','No') DEFAULT NULL,
  `pregnancy` enum('Yes','No') DEFAULT NULL,
  `report_status` enum('0','1') NOT NULL DEFAULT '1',
  `update_status` tinyint(1) NOT NULL,
  `technique` text NOT NULL,
  `impression` text NOT NULL,
  `r_breast` text NOT NULL,
  `l_breast` text NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `report_form`
--

INSERT INTO `report_form` (`rf_id`, `token`, `date`, `name`, `surname`, `age`, `address`, `f_number`, `h_number`, `district_idFK`, `tehsil_idFK`, `status`, `child`, `height`, `weight`, `bmi`, `lmp_date`, `test`, `out_come`, `chest`, `sc_date`, `investi`, `doc_file`, `mr_no`, `post_meno`, `pre_meno`, `pregnancy`, `report_status`, `update_status`, `technique`, `impression`, `r_breast`, `l_breast`, `user_id`) VALUES
(366, '8vXGFE1Z', '2016-12-27', 'Test', 'test', '18', 'Lahore', '923071234567', '46546545445', 25, 97, 'married', 4, '55', 45, 20, '2016-12-29', 'screening', 'Normal Mammography', 'left', '2015-12-27', 'Call for Biopsy', 'tasty.zip', '1032', 'Yes', 'No', 'Yes', '1', 1, '', '', '												', '												', 0),
(370, 'g3Utbl9F', '2016-12-28', 'jdfksdj', 'kdfjksd', '19', 'Lahore', '923071234567', '46546545445', 2, 5, 'married', 3, '55', 55, 10, '2017-01-21', 'screening', 'LCIS', 'left', '2016-12-29', 'Call for Biopsy', '', '123', 'No', 'No', 'No', '1', 1, '', '', '', '', 1),
(371, 'qSuO4IWc', '2016-12-28', 'shaan207', 'Mehmood', '20', 'Lahore', '923071234567', '46546545445', 34, 133, 'single', 0, '55', 55, 23, '2016-12-28', 'screening', 'Normal Mammography', 'left', '2016-12-28', 'Recall imaging', '', '13247', 'Yes', 'Yes', 'Yes', '1', 1, '', '', '', '', 4),
(372, 'p6UPJ2cz', '2016-12-29', 'Test', '', '', '', '', '', 24, 93, NULL, 0, '', 0, 0, '0000-00-00', NULL, 'Normal Mammography', 'left', '0000-00-00', NULL, '', '132454', NULL, NULL, NULL, '1', 1, '', '', '', '', 2),
(373, 'JUSW2Th0', '2016-12-29', 'shaan207', 'Mehmood', '22', 'Lahore', '923071234567', '46546545445', 9, 37, 'married', 1, '55', 55, 24, '1900-12-29', 'screening', 'DCIS', 'left', '1900-12-30', 'Call for Biopsy', '', '13245', 'Yes', 'Yes', 'Yes', '1', 1, '', '', '', '', 2),
(374, 'SnhtPCdy', '2016-12-29', 'test', 'vnbjd', '19', '', '923071234567', '46546545445', 6, 24, 'married', 1, '55', 0, 11, '0000-11-30', 'diagnostic', 'DCIS', 'left', '1900-12-30', 'Call for Biopsy', 'entrust-master.zip', '151512', 'Yes', 'Yes', 'Yes', '1', 1, '', '', '', '', 2),
(375, 'Po0MblBL', '2016-12-29', '', '', '', '', '', '', 24, 93, NULL, 0, '', 0, 0, '0000-00-00', NULL, 'LCIS', 'left', '0000-00-00', NULL, 'hp-20_7.zip', '3543543', NULL, NULL, NULL, '1', 1, '', '', '', '', 2),
(376, '8iUJY2f4', '2016-12-31', 'Final Test', 'Final test', '20', 'Lahore', '923071234567', '46546545445', 2, 8, 'married', 5, '55', 55, 12, '2016-12-31', 'diagnostic', 'DCIS', 'left', '2016-12-31', 'Call for Biopsy', 'tasty1.zip', '001', 'Yes', 'Yes', 'Yes', '1', 1, 'CC & MLO views of both breasts reviewed. ', 'Essentially unremarkable study------- BI-RADS assessment category - 1.', 'Glandular breast parenchyma is seen. Normal skin thickness is noted. Nipple is at its normal position. No mass lesion is seen. Retro glandular tissues are normal. No evidence of micro / macro calcification or architectural distortion is seen. No axillary lymph nodes are seen						', 'Fibroglandular breast parenchyma is seen. Normal skin thickness is noted. Nipple is at its normal position. No mass lesion is seen. Retroglandular tissues are normal. No evidence of micro / macro calcification or architectural distortion is seen. No  axillary lymph nodes are seen. 					', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tehsils`
--

CREATE TABLE `tehsils` (
  `tehsil_id` int(11) NOT NULL,
  `district_idFk` int(11) DEFAULT NULL,
  `tehsil_name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tehsils`
--

INSERT INTO `tehsils` (`tehsil_id`, `district_idFk`, `tehsil_name`) VALUES
(1, 1, 'CHUNIAN'),
(2, 1, 'KASUR'),
(3, 1, 'KOT RADHA KISHAN'),
(4, 1, 'PATTOKI'),
(5, 2, 'LAHORE CANTT'),
(6, 2, 'LAHORE CITY'),
(7, 2, 'MODEL TOWN '),
(8, 2, 'RAIWIND '),
(9, 2, 'SHALIMAR'),
(10, 3, 'FEROZWALA'),
(11, 3, 'MURIDKE'),
(12, 3, 'SAFDARABAD'),
(13, 3, 'SHARAQPUR'),
(14, 3, 'SHEIKHUPURA'),
(15, 4, 'NANKANA SAHIB'),
(16, 4, 'SANGLA HILL'),
(17, 4, 'SHAHKOT'),
(18, 5, 'BAHAWALNAGAR'),
(19, 5, 'CHISHTIAN'),
(20, 5, 'FORT ABBAS'),
(21, 5, 'HAROONABAD'),
(22, 5, 'MINCHINABAD'),
(23, 6, 'AHMADPUR EAST'),
(24, 6, 'BAHAWALPUR'),
(25, 6, 'BAHAWALPUR SADDAR'),
(26, 6, 'HASILPUR'),
(27, 6, 'KHAIRPUR TAMEWALI'),
(28, 6, 'YAZMAN'),
(29, 7, 'KHANPUR'),
(30, 7, 'LIAQATPUR'),
(31, 7, 'RAHIMYAR KHAN'),
(32, 7, 'SADIQABAD'),
(33, 8, 'DE-EX.AREA OF D.G.KHAN'),
(34, 8, 'DERA GHAZI KHAN'),
(35, 8, 'KOT CHATTA'),
(36, 8, 'TAUNSA'),
(37, 9, 'CHAUBARA'),
(38, 9, 'KAROR LALISAN'),
(39, 9, 'LAYYAH'),
(40, 10, 'ALIPUR'),
(41, 10, 'JATOI'),
(42, 10, 'KOT ADU'),
(43, 10, 'MUZAFFARGARH'),
(44, 11, 'DE-EX.AREA OF RAJANPUR'),
(45, 11, 'JAMPUR'),
(46, 11, 'RAJANPUR'),
(47, 11, 'ROJHAN'),
(48, 12, 'CHAK JHUMARA'),
(49, 12, 'FAISALABAD CITY'),
(50, 12, 'FAISALABAD SADDAR'),
(51, 12, 'JARANWALA'),
(52, 12, 'SAMUNDARI'),
(53, 12, 'TANDLIAN WALA'),
(54, 13, '18-HAZARI'),
(55, 13, 'JHANG'),
(56, 13, 'SHORKOT'),
(57, 13, 'AHMAD PUR SIAL'),
(58, 14, 'GOJRA'),
(59, 14, 'PIR MAHAL'),
(60, 14, 'KAMALIA'),
(61, 14, 'TOBA TEK SINGH'),
(62, 15, 'BHOWANA'),
(63, 15, 'CHINIOT'),
(64, 15, 'LALIAN'),
(65, 16, 'GUJRANWALA City'),
(66, 16, 'GUJRANWALA SADDAR'),
(67, 16, 'KAMOKE'),
(68, 16, 'NOSHERA VIRKAN'),
(69, 16, 'WAZIRABAD'),
(70, 17, 'GUJRAT'),
(71, 17, 'KHARIAN'),
(72, 17, 'SARAI ALAM GIR'),
(73, 18, 'DASKA'),
(74, 18, 'PASRUR'),
(75, 18, 'SAMBRIAL'),
(76, 18, 'SIALKOT'),
(77, 19, 'NAROWAL'),
(78, 19, 'SHAKARGARH'),
(79, 19, 'ZAFARWAL'),
(80, 20, 'HAFIZABAD'),
(81, 20, 'PINDI BHATTIAN'),
(82, 21, 'MALIKWAL'),
(83, 21, 'MANDI BAHUDDIN'),
(84, 21, 'PHALIA'),
(85, 22, 'JALALPUR PIRWALA'),
(86, 22, 'MULTAN CITY'),
(87, 22, 'MULTAN SADAR'),
(88, 22, 'SHUJA ABAD'),
(89, 23, 'BUREWALA'),
(90, 23, 'MAILSI'),
(91, 23, 'VEHARI'),
(92, 24, 'JAHANIAN'),
(93, 24, 'KABIRWALA'),
(94, 24, 'KHANEWAL'),
(95, 24, 'MIAN CHANNU'),
(96, 25, 'DUNYAPUR'),
(97, 25, 'KAROR PACCA'),
(98, 25, 'LODHRAN'),
(99, 26, 'ATTOCK'),
(100, 26, 'FATEH JANG'),
(101, 26, 'HASSANABDAL'),
(102, 26, 'HAZRO'),
(103, 26, 'JAND '),
(104, 26, 'PINDI GHEB'),
(105, 27, 'CHAKWAL'),
(106, 27, 'CHOA SAIDAN SHAH'),
(107, 27, 'KALLAR KAHAR'),
(108, 27, 'Lawa'),
(109, 27, 'TALAGANG'),
(110, 28, 'DINA'),
(111, 28, 'JHELUM'),
(112, 28, 'PIND DADAN KHAN'),
(113, 28, 'SOHAWA'),
(114, 29, 'GUJAR KHAN'),
(115, 29, 'KAHUTA'),
(116, 29, 'KALLAR SYEDAN'),
(117, 29, 'KOTLI SATTIAN'),
(118, 29, 'MURREE'),
(119, 29, 'RAWALPINDI'),
(120, 29, 'TAXILA'),
(121, 30, 'DEPALPUR'),
(122, 30, 'OKARA'),
(123, 30, 'RENALA KHURD'),
(124, 31, 'SAHIWAL'),
(125, 31, 'CHICHAWATANI'),
(126, 32, 'ARIFWALA'),
(127, 32, 'PAKPATTAN'),
(128, 33, 'BHAKKAR'),
(129, 33, 'DARYA KHAN'),
(130, 33, 'KALLUR KOT'),
(131, 33, 'MANKERA'),
(132, 34, 'KHUSHAB'),
(133, 34, 'NOORPUR THAL'),
(134, 34, 'QUAIDABAD'),
(135, 35, 'ISA KHEL'),
(136, 35, 'MIANWALI'),
(137, 35, 'PIPLAN'),
(138, 36, 'BHALWAL'),
(139, 36, 'BHERA'),
(140, 36, 'KOT MOMIN'),
(141, 36, 'SAHIWAL'),
(142, 36, 'SARGODHA'),
(143, 36, 'SHAHPUR'),
(144, 36, 'SILLANWALI');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(10) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `is_admin` int(10) NOT NULL,
  `role_title` enum('admin','CCH','ALRAZI','MOBILE_VAN') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `firstname`, `lastname`, `email`, `password`, `is_admin`, `role_title`) VALUES
(1, 'Administrator', 'Director', 'admin@cch.com', '3323fe11e9595c09af38fe67567a9394', 1, 'admin'),
(2, 'Mobile Van', 'One', 'mvone@cch-rc.com', '3323fe11e9595c09af38fe67567a9394', 1, 'MOBILE_VAN'),
(3, 'Mobile Van', 'Two', 'mvtwo@cch-rc.com', '3b712de48137572f3849aabd5666a4e3', 1, 'MOBILE_VAN'),
(4, 'Hijaz', 'One', 'hijaz@cch-rc.com', '4adcaece6cadbb6ce8047949b2f713f5', 1, 'CCH'),
(5, 'Al-Razi', 'Al-Razi', 'alrazi@cch-rc.com', 'b9dfbcc8536ee9b7a0ad5bbd14e90891', 0, 'ALRAZI');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `district`
--
ALTER TABLE `district`
  ADD PRIMARY KEY (`district_id`);

--
-- Indexes for table `finding_details`
--
ALTER TABLE `finding_details`
  ADD PRIMARY KEY (`fd_id`),
  ADD KEY `chest_idFK` (`rf_idFK`),
  ADD KEY `rf_idFK` (`rf_idFK`);

--
-- Indexes for table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`image_id`),
  ADD KEY `rf_idFK` (`rf_idFK`);

--
-- Indexes for table `report_form`
--
ALTER TABLE `report_form`
  ADD PRIMARY KEY (`rf_id`),
  ADD KEY `tahsel_idFK` (`district_idFK`),
  ADD KEY `tahsel_idFK_2` (`district_idFK`),
  ADD KEY `tahsil_idFK` (`tehsil_idFK`);

--
-- Indexes for table `tehsils`
--
ALTER TABLE `tehsils`
  ADD PRIMARY KEY (`tehsil_id`),
  ADD KEY `district_idFk` (`district_idFk`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `district`
--
ALTER TABLE `district`
  MODIFY `district_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `finding_details`
--
ALTER TABLE `finding_details`
  MODIFY `fd_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
  MODIFY `image_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `report_form`
--
ALTER TABLE `report_form`
  MODIFY `rf_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=377;
--
-- AUTO_INCREMENT for table `tehsils`
--
ALTER TABLE `tehsils`
  MODIFY `tehsil_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=145;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `finding_details`
--
ALTER TABLE `finding_details`
  ADD CONSTRAINT `finding_details_ibfk_1` FOREIGN KEY (`rf_idFK`) REFERENCES `report_form` (`rf_id`);

--
-- Constraints for table `image`
--
ALTER TABLE `image`
  ADD CONSTRAINT `image_ibfk_1` FOREIGN KEY (`rf_idFK`) REFERENCES `report_form` (`rf_id`);

--
-- Constraints for table `report_form`
--
ALTER TABLE `report_form`
  ADD CONSTRAINT `report_form_ibfk_1` FOREIGN KEY (`district_idFK`) REFERENCES `district` (`district_id`),
  ADD CONSTRAINT `report_form_ibfk_2` FOREIGN KEY (`tehsil_idFK`) REFERENCES `tehsils` (`tehsil_id`);

--
-- Constraints for table `tehsils`
--
ALTER TABLE `tehsils`
  ADD CONSTRAINT `tehsils_ibfk_1` FOREIGN KEY (`district_idFk`) REFERENCES `district` (`district_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
