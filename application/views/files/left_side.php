<div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <? $is_admin = $this->session->userdata('is_admin');
                                if($is_admin == 0){
                                    $avatar = 'user.png';
                                }else{
                                    $avatar = 'admin.png';
                                } ?>
                            <img src="<?= base_url('assets/images/'.$avatar)?>" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>
                            <? $firstname = $this->session->userdata('firstname');
                                echo "Hello, $firstname";
                                ?>
                            </p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu" >
                    <?                       
                        $display = 'display:none;';
                        
                        $role = $this->session->userdata('role');
                        
                        if($role == 'CCH' || $role == 'MOBILE_VAN' || $role == 'admin'){
                            $display = 'display:block;';
                        }
                        ?>
                        <li class="<?php if($this->uri->segment(2)=="add_report"){echo "active";}?>" style="<?=$display;?>">
                            <a href="<?= base_url().'main/add_report'?>" >
                                <i class="fa fa-edit"></i> <span>Add Report</span>
                            </a>
                        </li>

                        <li class="<?php if($this->uri->segment(2)=="latest_report" || $this->uri->segment(2)=="admin"){echo "active";}?>" >
                            <a href="<?= base_url().'main/latest_report'?>" >
                                <i class="fa fa-file-text-o"></i> <span>Latest Reports</span>
                            </a>
                        </li>

                        <li class="<?php if($this->uri->segment(2)=="all_reports" || $this->uri->segment(2)=="admin"){echo "active";}?>">
                            <a href="<?= base_url().'main/all_reports'?>" >
                                <i class="fa fa-file-text-o"></i> <span>All Reports</span>
                            </a>
                        </li>

                        <?
                        $display = 'display:none;';
                        
                        $role = $this->session->userdata('role');
                        
                        if($role == 'admin'){
                            $display = 'display:block;';
                        }
                        ?>
                        <li style="<?=$display;?>" class="<?php if($this->uri->segment(2)=="create_user"){echo "active";}?>">
                            <a href="<?= base_url().'main/create_user'?>">
                                <i class="fa fa-user"></i> <span>Add User</span>
                            </a>
                        </li>

                        <li style="<?=$display;?>" class="<?php if($this->uri->segment(2)=="all_users"){echo "active";}?>">
                            <a href="<?= base_url().'main/all_users'?>">
                                <i class="fa fa-users"></i> <span>All Users</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>