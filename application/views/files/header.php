<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Director</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta name="description" content="Developed By M Abdur Rokib Promy">
    <meta name="keywords" content="Admin, Bootstrap 3, Template, Theme, Responsive">
    <!-- bootstrap 3.0.2 -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/bootstrap.min.css')?>">
    <!-- font Awesome -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/font-awesome.min.css')?>">
    <!-- Ionicons -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/ionicons.min.css')?>">
    <!-- Morris chart -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/morris/morris.css')?>">
    <!-- jvectormap -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/jvectormap/jquery-jvectormap-1.2.2.css')?>">
    <!-- Date Picker -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/datepicker/datepicker3.css')?>">
    <!-- fullCalendar -->
    <!-- <link href="css/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" /> -->
    <!-- Daterange picker -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/daterangepicker/daterangepicker-bs3.css')?>">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/iCheck/all.css')?>">
    <!-- data table min.css -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/datatable.min.css')?>">
    <!-- bootstrap wysihtml5 - text editor -->
    <!-- <link href="css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" /> -->
    <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <!-- Theme style -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/style.css')?>">
    <!-- Toastr -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/toastr.min.css')?>">

    <script src="<?= base_url('assets/js/jquery.min.js')?>"></script>
    <script src="<?= base_url('/assets/js/ZipApi/jszip.min.js')?>"></script>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
          <![endif]-->

          <style type="text/css">

          </style>
      </head>
      <body class="skin-black">