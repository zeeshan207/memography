
<!-- jQuery 2.0.2 -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
        

        <!-- jQuery UI 1.10.3 -->
        <script src="<?= base_url('assets/js/jquery-ui-1.10.3.min.js')?>"></script>
        <!-- Bootstrap -->
        <script src="<?= base_url('assets/js/bootstrap.min.js')?>"></script>
        <!-- daterangepicker -->
        <script src="<?= base_url('assets/js/plugins/daterangepicker/daterangepicker.js')?>"></script>

        <script src="<?= base_url('assets/js/plugins/chart.js')?>"></script>

         <!-- datepicker -->
        <script src="<?= base_url('assets/js/plugins/datepicker/bootstrap-datepicker.js')?>" type="text/javascript"></script>
        <!-- Bootstrap WYSIHTML5
        <script src="js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>-->
        <!-- iCheck -->
        <script src="<?= base_url('assets/js/plugins/iCheck/icheck.min.js')?>"></script>
        <!-- calendar -->
        <script src="<?= base_url('assets/js/plugins/fullcalendar/fullcalendar.js')?>"></script>
        <!-- toastr -->
        <script src="<?= base_url('assets/js/toastr.min.js')?>"></script>
        <!-- for data table jquery plugin -->
        <script src="<?= base_url('assets/js/datatable.min.js')?>"></script>
        <!-- Director App -->
        <script src="<?= base_url('assets/js/Director/app.js')?>"></script>
        <!-- Custom file -->
        <script src="<?= base_url('assets/js/custom.js')?>"></script>

        <!-- Director for demo purposes -->
        <script type="text/javascript">
            $('input').on('ifChecked', function(event) {
                // var element = $(this).parent().find('input:checkbox:first');
                // element.parent().parent().parent().addClass('highlight');
                $(this).parents('li').addClass("task-done");
                console.log('ok');
            });
            $('input').on('ifUnchecked', function(event) {
                // var element = $(this).parent().find('input:checkbox:first');
                // element.parent().parent().parent().removeClass('highlight');
                $(this).parents('li').removeClass("task-done");
                console.log('not');
            });

        </script>
        <script>
            $('#noti-box').slimScroll({
                height: '400px',
                size: '5px',
                BorderRadius: '5px'
            });

            $('input[type="checkbox"].flat-grey, input[type="radio"].flat-grey').iCheck({
                checkboxClass: 'icheckbox_flat-grey',
                radioClass: 'iradio_flat-grey'
            });
</script>
<script type="text/javascript">
    $(function() {
                "use strict";
                //BAR CHART
                var data = {
                    labels: ["January", "February", "March", "April", "May", "June", "July"],
                    datasets: [
                        {
                            label: "My First dataset",
                            fillColor: "rgba(220,220,220,0.2)",
                            strokeColor: "rgba(220,220,220,1)",
                            pointColor: "rgba(220,220,220,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(220,220,220,1)",
                            data: [65, 59, 80, 81, 56, 55, 40]
                        },
                        {
                            label: "My Second dataset",
                            fillColor: "rgba(151,187,205,0.2)",
                            strokeColor: "rgba(151,187,205,1)",
                            pointColor: "rgba(151,187,205,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(151,187,205,1)",
                            data: [28, 48, 40, 19, 86, 27, 90]
                        }
                    ]
                };
                // new Chart(document.getElementById("linechart").getContext("2d")).Line(data,{
                //     responsive : true,
                //     maintainAspectRatio: false,
                // });

            });
    // toastr section here
    <? if ($this->session->flashdata('login_success')) {
        $firstname = $this->session->userdata('firstname'); ?>

        toastr.success('Thanks..', 'Welcome to <?= $firstname; ?>!');
       
    <? }elseif ($this->session->flashdata('login_failed')) {?>

        toastr.error('Please try again..', 'Email or Password error!');
       
    <? }elseif ($this->session->flashdata('create_error')) {?>

        toastr.error('Please try again..', 'Create user error!');
       
    <? }elseif ($this->session->flashdata('create_success')) {?>

       toastr.success('Thanks..', 'User Succesfuly added!');
       
    <? }elseif ($this->session->flashdata('update_user_error')) {?>

        toastr.error('Please try again..', 'Update user error!');
       
    <? }elseif ($this->session->flashdata('update_user_success')) {?>

       toastr.success('Thanks..', 'User Succesfuly Updated!');
       
    <? }elseif ($this->session->flashdata('user_delete_success')) {?>

       toastr.success('Request proccesed', 'User deleted!');
       
    <? }elseif ($this->session->flashdata('user_delete_error')) {?>

       toastr.error('Try Again..', 'Cannot delete!');
       
    <? }elseif ($this->session->flashdata('logout_success')) {?>

       toastr.success('Thanks..', 'Logging Out Seccusfull!');
       
    <? }elseif ($this->session->flashdata('report_error')) {?>

       toastr.error('Try Again..', 'Cannot added!');
       
    <? }elseif ($this->session->flashdata('report_success')) {?>

       toastr.success('Thanks..', 'Report Submit!');
       
    <? }elseif ($this->session->flashdata('report_update_error')) {?>

       toastr.error('Try Again..', 'Cannot Updated!');
       
    <? }elseif ($this->session->flashdata('report_update_success')) {?>

       toastr.success('Thanks..', 'Report Updated!');
       
    <? } ?>
    
    $('#sc-date').datepicker({
        format: 'yyyy/mm/dd'
    });
    $('#lmp-date').datepicker({
        format: 'yyyy/mm/dd'
    });

    $(document).ready(function(){
        $('.table').DataTable();

        $('#newEmail').click(function(){
            $( "#newEmailField" ).toggle( 'display' );
            if ( display === true ) {
              $( "#newEmailField" ).show();
            } else if ( display === false ) {
              $( "#newEmailField" ).hide();
            }
        });
    });


     $('#email').blur(function(){

        var email = $("#email").val();
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        
        if(email != '' && filter.test(email) ){


          $.ajax({
            type: "POST",
            url: "<?=base_url()?>main/check_email_availablity",
            data: {
              'email': email
            },
            dataType: 'json'
          }).success(function(res) {


            if(res.response){

                document.getElementById('message').innerHTML = "Success";
                $('#message').removeClass('label label-danger').addClass('label label-success');

              }
              else{
                $("#email").val("");
                document.getElementById('message').innerHTML = "Already Exist.";
                $('#message').removeClass('label label-success').addClass('label label-danger');
                $('#email').attr('placeholder', 'Re Enter A Valid Email');
              }

          });

        }

      });
     // for new email 
     $('#newemail').blur(function(){

        var email = $("#newemail").val();
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        
        if(email != '' && filter.test(email) ){


          $.ajax({
            type: "POST",
            url: "<?=base_url()?>main/check_email_availablity",
            data: {
              'email': email
            },
            dataType: 'json'
          }).success(function(res) {


            if(res.response){

                document.getElementById('newmessage').innerHTML = "Success";
                $('#newmessage').removeClass('label label-danger').addClass('label label-success');

              }
              else{
                $("#newemail").val("");
                document.getElementById('newmessage').innerHTML = "Already Exist.";
                $('#newmessage').removeClass('label label-success').addClass('label label-danger');
                $('#newemail').attr('placeholder', 'Re Enter A Valid Email');
              }

          });

        }

      });
     // for get telsils 
     $('#district').change(function(){

        var dis_id = $('#district').val();
        $('#tehsil').html('');

          $.ajax({
            type: "GET",
            url: "<?=base_url()?>main/get_tehsils/"+dis_id,
            dataType: ''
          }).success(function(res) {

            $('#tehsil').append(res);

          });


      });

    // check password matched or not
    $('.cpassword').focusout(function(){
        var pass = $('.fpassword').val();
        var cpass = $('.cpassword').val();

        if(pass != cpass){

            $('.checkpass').text("Password do not matched");
            $('.checkpass').removeClass('label label-success').addClass('label label-danger');
            $(".cpassword").val("");
            $('.cpassword').attr('placeholder', 'Re Enter A Confirm Password');

        }else{

            $('.checkpass').text("Password matched");
            $('.checkpass').removeClass('label label-danger').addClass('label label-success');

        }
    });
    // start here status is married or single
     $('input[name="status"]').change(function() {

        if ($(this).is(':checked') && $(this).val() == 'married') {
            console.log($(this).val());
            $('#noofchild').css('display', 'block');
        }
        if ($(this).is(':checked') && $(this).val() == 'single') {
            $('#noofchild').css('display', 'none');
        }

    });

     // current date on report form
        var d = new Date();

        var month = d.getMonth()+1;
        var day = d.getDate();

        var output = d.getFullYear() + '/' +
            (month<10 ? '0' : '') + month + '/' +
            (day<10 ? '0' : '') + day;
        $('#date').val(output);
    //  view report next or prev
    $('.print').click(function(){
        $('.non-printable').hide();
        window.print();
        $('.non-printable').show();
    });
    

    $('#img').change(function(){
        

        var img = $(this).prop("files")[0];
        var form_data = new FormData();
        form_data.append("files", img);
        
        $.ajax({
            data: form_data,
            processData: false,
            contentType: false,
            url: "<?=base_url()?>main/do_upload",
            type: "POST",
            dataType: "",
          }).success(function(res) {

            $('#imagas').append(res);

            img_count();

        });

    });

    $('body').delegate('#img-close','click',function(){
        
        var name = $(this).attr('data-name');
        
        $.ajax({
            url: "<?=base_url()?>main/delete_img/"+name,
            type: "POST",
            dataType: "",
          }).success(function(res) {
            
            

        });

        $(this).parent().remove();
        img_count();

        
    });


    function img_count(){
        var images = [];
        $('input[type=hidden]').each(function(){
            images.push($(this).val());
        });
        if(images.length == 4){
            $('#img').hide();
        }else{
            $('#img').show();
        }
    }


</script>
</body>
</html>