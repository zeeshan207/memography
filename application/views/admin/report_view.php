	<?= $header?>


<div class="container non-printable">
	<? $margin_right = 'style="margin-right: 5px;"'; $text_align = 'style="text-align:right;"'; ?>
	<div class="row" style="margin-top: 10px;">
		<div class="col-sm-1">
			<a class="btn btn-primary print">
                <i class="fa fa-print" <?= $margin_right; ?>></i> <span>Print</span>
            </a>
		</div>
	<? $is_admin = $this->session->userdata('is_admin');
        if($is_admin == 1){?>
		<div class="col-sm-1 col-sm-offset-8" <?= $text_align; ?> >
			<a class="btn btn-primary" href="<?= base_url().'main/edit_report/'.$current?>">
                <i class="fa fa-edit" <?= $margin_right; ?>></i> <span> Edit Report</span>
            </a>
		</div>
		<div class="col-sm-2"  <?= $text_align; ?>>
			<a class="btn btn-primary" href="<?= base_url().'main/add_report'?>">
                <i class="fa fa-file" <?= $margin_right; ?>></i> <span> Add Report</span>
            </a>
		</div>
	</div>
	<?}?>
</div>

<div class="container">
	<? foreach ($report as $rep) {?>
	<section class="content">
			<div class="row">
				<div class="col-sm-12">
					<section class="panel">
				      <!-- <header class="panel-heading">hello</header> -->
				      <div class="panel-body">
							<div class="row">
								<div class="col-sm-12"></div>
							</div>
							<div class="row" style="height: 300px;"></div>
							<div class="row">
								<div class="col-sm-3 col-xs-3"></div>
								<div class="col-sm-5 col-xs-5">
									<div class="col-sm-6 col-xs-6">
										<h5><b>Patient's Name:</b></h5>
									</div>
									<div class="col-sm-6 col-xs-6">
										<h6><?=$rep['name']; ?></h6>
									</div>
								</div>

								<div class="col-sm-4 col-xs-4">
									<div class="col-sm-4 col-xs-4">
										<h5><b>Date:</b></h5>
									</div>
									<div class="col-sm-8 col-xs-8">
										<h6><?=$rep['date'];?></h6>
									</div>
								</div>
							</div>
							
							<div class="row">	
								<div class="col-sm-3 col-xs-3"></div>
								<div class="col-sm-5 col-xs-5">
									<div class="col-sm-6 col-xs-6">
										<h5><b>Age:</b></h5>
									</div>
									<div class="col-sm-6 col-xs-6">
										<h6><?=$rep['age']; ?> Years</h6>
									</div>
								</div>

								<div class="col-sm-4 col-xs-4">
									<div class="col-sm-6 col-xs-6">
										<h5><b>MR Number:</b></h5>
									</div>
									<div class="col-sm-6 col-xs-6">
										<h6><?=$rep['mr_no'];?></h6>
									</div>
								</div>

							</div>

							
							<div class="row">
								<div class="col-sm-3 col-xs-3"></div>
								<div class="col-sm-9 col-xs-9" align="center" style="text-decoration: underline">
									<h4>BILATERAL MAMMOGRAPHY REPORT</h4>
								</div>

								<!-- Technique -->
								<div class="col-sm-3 col-xs-3"></div>
								<div class="col-sm-9 col-xs-9">
									<div class="col-sm-12">
										<h5><b>Technique:</b></h5>
									</div>
									<div class="col-sm-12">
										<h5><?=$rep['technique']; ?></b></h5>
									</div>
								</div>

								<!-- Report Heading -->
								<div class="col-sm-3 col-xs-3"></div>
								<div class="col-sm-9 col-xs-9">
									<div class="col-sm-12">
										<h5><b>Report:</b></h5>
									</div>
								</div>

								<!-- Right Breast -->
								<div class="col-sm-3 col-xs-3"></div>
								<div class="col-sm-9 col-xs-9">
									<div class="col-sm-12">
										<h5><b>RIGHT BREAST:</b></h5>
									</div>
									<div class="col-sm-12">
										<h5><?=$rep['r_breast']; ?></b></h5>
									</div>
								</div>


								<!-- Left Breast -->
								<div class="col-sm-3 col-xs-3"></div>
								<div class="col-sm-9 col-xs-9">
									<div class="col-sm-12">
										<h5><b>LEFT BREAST:</b></h5>
									</div>
									<div class="col-sm-12">
										<h5><?=$rep['l_breast']; ?></b></h5>
									</div>
								</div>

								<!-- IMPRESSION -->
								<div class="col-sm-3 col-xs-3"></div>
								<div class="col-sm-9 col-xs-9">
									<div class="col-sm-12">
										<h5><b>IMPRESSION:</b></h5>
									</div>
									<div class="col-sm-12">
										<h5><?=$rep['impression']; ?></b></h5>
									</div>
								</div>


								<!-- Bottom Note -->
								<div class="col-sm-3 col-xs-3"></div>
								<div class="col-sm-9 col-xs-9">
									<p>* See reverse for details of assessment categories.</p>
								
								</div>

							</div>							
				        </div>
				    </section>
				</div>
			</div>
	</section>

	<section class="content non-printable">
		<div class="row">
			<div class="col-sm-12">
				<section class="panel">
			      <header class="panel-heading">
			          Recommendation
			      </header>
			      <div class="panel-body">
			      		<div class="row">
							<div class="col-sm-4 col-xs-6">
								<div class="col-sm-6 col-xs-6">
									<h6>Screening Date</h6>
								</div>
								<div class="col-sm-6 col-xs-6" style="border-right: 1px dotted black;">
									<h5><b><?=$rep['sc_date'];?></b></h5>
								</div>
							</div>
							<div class="col-sm-4 col-xs-6">
								<div class="col-sm-6 col-xs-6">
									<h6>Investigation</h6>
								</div>
								<div class="col-sm-6 col-xs-6" style="border-right: 1px dotted black;">
									<h5><b><?=$rep['investi'];?></b></h5>
								</div>
							</div>
							<?if($rep['doc_file'] != ""){ ?>
							<div class="col-sm-4 non-printable">
								<div class="col-sm-6 col-xs-6">
									<h6>Document File</h6>
								</div>
								<div class="col-sm-6 col-xs-6">
									<a class="btn btn-info" download="" href="<?=base_url()."assets/uploads/files/".$rep['doc_file']?>">Download File</a>
								</div>
							</div>
							<?}?>
						</div>
						<?}?>
			      </div>
			    </section>
			</div>
		</div>
	</section>
	<?if($images == true){?>
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<section class="panel">
			      <header class="panel-heading">
			          Memo Images
			      </header>
			      <div class="panel-body">
			      		<div class="row">
							<? foreach ($images as $image) {?>
								<div class="col-sm-3">
									<div class="profile-img" style="background-image: url(<?=base_url()."assets/uploads/images/".$image['image_name'];?> ); ">
										
									</div>
								</div>
							<? } ?>
						</div>
			      </div>
			    </section>
			</div>
		</div>
	</section>
	<?}?>
	<? if($this->uri->segment(2)=="report_view"){ ?>
	<div class="row non-printable" style="margin-bottom: 25px;">
		<div class="col-sm-2">
			<?if($prev != ""){?>
				<a href="<?= base_url().'main/report_view/'.$prev?>"><i class="fa fa-arrow-left" <?= $margin_right; ?>></i>Previous</a>
			<?}?>
		</div>
		<div class="col-sm-2 col-sm-offset-8" <?= $text_align; ?>>
			<? if($next != "") {?>
				<a href="<?= base_url().'main/report_view/'.$next?>" class="next">Next<i class="fa fa-arrow-right" style="margin-left:5px;"></i></a>
			<?}?>
		</div>
	</div>
	<?}?>
</div>

<?= $footer?>