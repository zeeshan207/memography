

<? foreach($get_result as $report) {?>
<tr>
	<td>
		<?= ++$count ?>
	</td>
	<td>
		<?= $report['token']; ?>
	</td>
	<td>
		<?= $report['name']; ?>
	</td>
	<td>
		<div class="row">
			<div class="col-lg-3">
				<?= anchor("main/view_report/".$report['rf_id'],'View',['class'=>'btn btn-primary', 'target' => '_blank']);?>
			</div>
		</div>
	</td>
</tr>
<?	} ?>
<? }else {?>
	<tr>
		<td colspan="3">
			No Records Found.
		</td>
	</tr>
<? } ?>