<?= $header?>

<?= $head?>
<?= $left_side?>

<aside class="right-side">
	<!-- Content Header (Page header) -->


	<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-lg-12">
					<table class="table">
						<thead>
							<tr role='row'>
								<th class="col-lg-1">Sr. No.</th>
								<th class="col-lg-2">Name</th>
								<th class="col-lg-2">Role</th>
								<th class="col-lg-4">Email Address</th>
								<th class="col-lg-3">Action</th>
							</tr>
						</thead>
						<tbody>
						<? if ( count($users) ) { 
							$count = $this->uri->segment(3, 0);
						 	foreach($users as $user) { ?>
							<tr>
								<td>
									<?= ++$count ?>
								</td>
								<td>
									<?= $user['firstname']; ?>
								</td>
								<td>
									<?= $user['role_title']; ?>
								</td>
								<td>
									<?= $user['email']; ?>
								</td>
								<td>
									<div class="row">
										<div class="col-lg-3">
											<?= anchor("main/edit_user/".$user['user_id'],'Edit',['class'=>'btn btn-primary']);?>
										</div>
										<?if ($user['user_id'] != 1 ){?>
											<div class="col-lg-3">
												<?= form_open('main/delete_user'),
													form_hidden('user_id', $user['user_id']),
													form_submit(['title'=>'Are You Sure To Delete User!.','value'=>'Delete','class'=>'btn btn-danger']),
													form_close();
												?>
											</div>
										<?}?>
									</div>
								</td>
							</tr>
							<?	} ?>
						<? }else {?>
							<tr>
								<td colspan="4">
									No Records Found.
								</td>
							</tr>
						<? } ?>
						</tbody>
					</table>
				</div>
			</div>
		</section>
	</aside>
</div>
<?= $footer?>