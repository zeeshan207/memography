<?= $header?>

<div class="container non-printable">
	<? $margin_right = 'style="margin-right: 5px;"'; $text_align = 'style="text-align:right;"'; ?>
	<div class="row" style="margin-top: 10px;">
		<div class="col-lg-1">
			<a class="btn btn-primary print">
                <i class="fa fa-print" <?= $margin_right; ?>></i> <span>Print</span>
            </a>
		</div>
	<? $is_admin = $this->session->userdata('is_admin');
        if($is_admin == 1){?>
		<div class="col-lg-1 col-lg-offset-8" <?= $text_align; ?> >
			<a class="btn btn-primary" href="<?= base_url().'main/edit_report/'.$current?>">
                <i class="fa fa-edit" <?= $margin_right; ?>></i> <span> Edit Report</span>
            </a>
		</div>
		<div class="col-lg-2"  <?= $text_align; ?>>
			<a class="btn btn-primary" href="<?= base_url().'main/add_report'?>">
                <i class="fa fa-file" <?= $margin_right; ?>></i> <span> Add Report</span>
            </a>
		</div>
	</div>
	<?}?>
</div>
<div class="container">
	<? foreach ($report as $rep) {?>
	<section class="content">
			<div class="row">
				<div class="col-lg-12">
					<section class="panel">
				      <header class="panel-heading">
				          Memo Number
				          <span style=" margin-left: 10px; "><?=$rep['mr_no'];?></span>
				          <span style="float: right;"><?=$rep['date'];?></span>
				      </header>
				      <div class="panel-body">
							<div class="row">
								<div class="col-lg-12">
									<h4 style="border-bottom: 1px solid black;">Patient Details</h4>
								</div>
							</div>
							<div class="row" style="margin-bottom: 8px;">
								<div class="col-lg-4 col-xs-6">
									<div class="col-lg-6 col-xs-6">
										<h6>Name</h6>
									</div>
									<div class="col-lg-6 col-xs-6" style="border-right: 1px dotted black;">
										<h5><b><?=$rep['name']; ?></b></h5>
									</div>
								</div>
								<div class="col-lg-4 col-xs-6">
									<div class="col-lg-6 col-xs-6">
										<h6>W/O D/O</h6>
									</div>
									<div class="col-lg-6 col-xs-6" style="border-right: 1px dotted black;">
										<h5><b><?=$rep['surname']; ?></b></h5>
									</div>
								</div>
								<div class="col-lg-4 col-xs-6">
									<div class="col-lg-6 col-xs-6">
										<h6>Age</h6>
									</div>
									<div class="col-lg-6 col-xs-6">
										<h5><b><?=$rep['age']; ?></b></h5>
									</div>
								</div>
							</div>
							<div class="row" style="margin-bottom: 8px;">
								<div class="col-lg-2 col-xs-6">
									<h6>Postal Address</h6>
								</div>
								<div class="col-lg-6 col-xs-6">
									<h5 style="border-right: 1px dotted black;"><b><?=$rep['address']; ?></b></h5>
								</div>
								<div class="col-lg-4 col-xs-6">
									<div class="col-lg-6 col-xs-6">
										<h6>District</h6>
									</div>
									<div class="col-lg-6 col-xs-6">
										<h5><b><?=$rep['district_name']; ?></b></h5>
									</div>
								</div>
							</div>
							<div class="row" style="margin-bottom: 8px;">
								<div class="col-lg-4 col-xs-6">
									<div class="col-lg-6 col-xs-6">
										<h6>Tehsil</h6>
									</div>
									<div class="col-lg-6 col-xs-6" style="border-right: 1px dotted black;">
										<h5><b><?=$rep['tehsil_name']; ?></b></h5>
									</div>
								</div>
								<div class="col-lg-4 col-xs-6">
									<div class="col-lg-6 col-xs-6">
										<h6>Contact Number</h6>
									</div>
									<div class="col-lg-6 col-xs-6" style="border-right: 1px dotted black;">
										<h5><b>+<?=$rep['f_number'];?></b></h5>
									</div>
								</div>
								<div class="col-lg-4 col-xs-6">
									<div class="col-lg-6 col-xs-6">
										<h6>CNIC#</h6>
									</div>
									<div class="col-lg-6 col-xs-6">
										<h5><b><?=$rep['h_number'];?></b></h5>
									</div>
								</div>
							</div>
							<div class="row" style="margin-bottom: 8px;">
								<div class="col-lg-4 col-xs-6">
									<div class="col-lg-6 col-xs-6">
										<h6>Status</h6>
									</div>
									<div class="col-lg-6 col-xs-6" style="border-right: 1px dotted black;">
										<h5><b><?=$rep['status']; ?></b></h5>
									</div>
								</div>
								<div class="col-lg-4 col-xs-6">
									<div class="col-lg-6 col-xs-6">
										<h6>Child</h6>
									</div>
									<div class="col-lg-6 col-xs-6" style="border-right: 1px dotted black;">
										<h5><b><?=$rep['child']; ?></b></h5>
									</div>
								</div>
								<div class="col-lg-4 col-xs-6">
									<div class="col-lg-6 col-xs-6">
										<h6>Height</h6>
									</div>
									<div class="col-lg-6 col-xs-6">
										<h5><b><?=$rep['height'];?> CM</b></h5>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-4 col-xs-6">
									<div class="col-lg-6 col-xs-6">
										<h6>Weight</h6>
									</div>
									<div class="col-lg-6 col-xs-6" style="border-right: 1px dotted black;">
										<h5><b><?=$rep['weight']; ?> Kg</b></h5>
									</div>
								</div>
								<div class="col-lg-4 col-xs-6">
									<div class="col-lg-6 col-xs-6">
										<h6>LMP Date</h6>
									</div>
									<div class="col-lg-6 col-xs-6" style="border-right: 1px dotted black;">
										<h5><b><?=$rep['lmp_date'];?></b></h5>
									</div>
								</div>
								<div class="col-lg-4 col-xs-6">
									<div class="col-lg-6 col-xs-6">
										<h6>BMI</h6>
									</div>
									<div class="col-lg-6 col-xs-6">
										<h5><b><?=$rep['bmi']; ?></b></h5>
									</div>
								</div>
							</div>
							<div class="row" style="margin-top: 8px;">
								<div class="col-lg-4 col-xs-6">
									<div class="col-lg-6 col-xs-6">
										<h6>Test</h6>
									</div>
									<div class="col-lg-6 col-xs-6" style="border-right: 1px dotted black;">
										<h5><b><?=$rep['test']; ?></b></h5>
									</div>
								</div>
								<div class="col-lg-4 col-xs-6">
									<div class="col-lg-6 col-xs-6">
										<h6>Post Menopausal</h6>
									</div>
									<div class="col-lg-6 col-xs-6" style="border-right: 1px dotted black;">
										<h5><b><?=$rep['post_meno']; ?></b></h5>
									</div>
								</div>
								<div class="col-lg-4 col-xs-6">
									<div class="col-lg-6 col-xs-6">
										<h6>Pre Menopausal</h6>
									</div>
									<div class="col-lg-6 col-xs-6">
										<h5><b><?=$rep['pre_meno']; ?></b></h5>
									</div>
								</div>
							</div>
							<div class="row" style="margin-top: 8px;">
								<div class="col-lg-4 col-xs-6">
									<div class="col-lg-6 col-xs-6">
										<h6>Pregnancy</h6>
									</div>
									<div class="col-lg-6 col-xs-6" style="border-right: 1px dotted black;">
										<h5><b><?=$rep['pregnancy']; ?></b></h5>
									</div>
								</div>
							</div>
							<!-- <div class="row">
								<div class="col-lg-12">
									<h4 style="border-bottom: 1px solid black;">Report Details</h4>
								</div>
							</div> -->
							<? foreach($rep['fd_details'] as $index => $val){ ?>
							<? $heading = "";
								if($index == '0'){
									$heading = 'Left';
								}else{
									$heading = 'Right';	
								} ?>
							<h5 style="border-bottom: 1px solid black;"><b><?=$heading;?> Breast</b></h5>
							<div class="row" style="margin-bottom: 8px;">
								<div class="col-lg-4 col-xs-6">
									<div class="col-lg-6 col-xs-6">
										<h6>Density</h6>
									</div>
									<div class="col-lg-6 col-xs-6" style="border-right: 1px dotted black;">
										<h5><b><?=$val['density']; ?></b></h5>
									</div>
								</div>
								<div class="col-lg-4 col-xs-6">
									<div class="col-lg-6 col-xs-6">
										<h6>Clutered Micro Calcification</h6>
									</div>
									<div class="col-lg-6 col-xs-6" style="border-right: 1px dotted black;">
										<h5><b><?=$val['cm_cal'];?></b></h5>
									</div>
								</div>
								<div class="col-lg-4 col-xs-6">
									<div class="col-lg-6 col-xs-6">
										<h6>Architectural Distortion</h6>
									</div>
									<div class="col-lg-6 col-xs-6">
										<h5><b><?=$val['a_dis'];?></b></h5>
									</div>
								</div>
							</div>
							<div class="row" style="margin-bottom: 8px;">
								<div class="col-lg-4 col-xs-6">
									<div class="col-lg-6 col-xs-6">
										<h6>Spiculated Focal Mass</h6>
									</div>
									<div class="col-lg-6 col-xs-6" style="border-right: 1px dotted black;">
										<h5><b><?=$val['focal_mass'][0];?> x <?=$val['focal_mass'][1];?></b></h5>
									</div>
								</div>
								<div class="col-lg-4 col-xs-6">
									<div class="col-lg-6 col-xs-6">
										<h6>Asymmetry</h6>
									</div>
									<div class="col-lg-6 col-xs-6" style="border-right: 1px dotted black;">
										<h5><b><?=$val['asymmetry'];?></b></h5>
									</div>
								</div>
								<div class="col-lg-4 col-xs-6">
									<div class="col-lg-6 col-xs-6">
										<h6>Quadrant</h6>
									</div>
									<div class="col-lg-6 col-xs-6">
										<h5><b><?=$val['quadrant'];?></b></h5>
									</div>
								</div>
							</div>
							<div class="row" style="margin-bottom: 8px;">
								<div class="col-lg-4 col-xs-6">
									<div class="col-lg-6 col-xs-6">
										<h6>Linear Branding Micro Calcification</h6>
									</div>
									<div class="col-lg-6 col-xs-6" style="border-right: 1px dotted black;">
										<h5><b><?=$val['lbm_cal'];?></b></h5>
									</div>
								</div>
								<div class="col-lg-4 col-xs-6">
									<div class="col-lg-6 col-xs-6">
										<h6>Birad</h6>
									</div>
									<div class="col-lg-6 col-xs-6" style="border-right: 1px dotted black;">
										<h5><b><?=$val['birad'];?></b></h5>
									</div>
								</div>
							</div>
							<? } ?>
				        </div>
				    </section>
				</div>
			</div>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-lg-12">
				<section class="panel">
			      <header class="panel-heading">
			          Recommendation
			      </header>
			      <div class="panel-body">
			      		<div class="row">
							<div class="col-lg-4 col-xs-6">
								<div class="col-lg-6 col-xs-6">
									<h6>Screening Date</h6>
								</div>
								<div class="col-lg-6 col-xs-6" style="border-right: 1px dotted black;">
									<h5><b><?=$rep['sc_date'];?></b></h5>
								</div>
							</div>
							<div class="col-lg-4 col-xs-6">
								<div class="col-lg-6 col-xs-6">
									<h6>Investigation</h6>
								</div>
								<div class="col-lg-6 col-xs-6" style="border-right: 1px dotted black;">
									<h5><b><?=$rep['investi'];?></b></h5>
								</div>
							</div>
							<?if($rep['doc_file'] != ""){ ?>
							<div class="col-lg-4  non-printable">
								<div class="col-lg-6">
									<h6>Document File</h6>
								</div>
								<div class="col-lg-6">
									<a class="btn btn-info" download="" href="<?=base_url()."assets/uploads/files/".$rep['doc_file']?>">Download File</a>
								</div>
							</div>
							<?}?>
						</div>
						<?}?>
			      </div>
			    </section>
			</div>
		</div>
	</section>
	<?if($images == true){?>
	<section class="content">
		<div class="row">
			<div class="col-lg-12">
				<section class="panel">
			      <header class="panel-heading">
			          Memo Images
			      </header>
			      <div class="panel-body">
			      		<div class="row">
							<? foreach ($images as $image) {?>
								<div class="col-lg-3  col-xs-6">
									<div class="profile-img" style="background-image: url(<?=base_url()."assets/uploads/images/".$image['image_name'];?> ); ">
										
									</div>
								</div>
							<? } ?>
						</div>
			      </div>
			    </section>
			</div>
		</div>
	</section>
	<?}?>
	<? if($this->uri->segment(2)=="view_report"){ ?>
	<div class="row non-printable" style="margin-bottom: 25px;">
		<div class="col-lg-2">
			<?if($prev != ""){?>
				<a href="<?= base_url().'main/view_report/'.$prev?>"><i class="fa fa-arrow-left" <?= $margin_right; ?>></i>Previous</a>
			<?}?>
		</div>
		<div class="col-lg-2 col-lg-offset-8" <?= $text_align; ?>>
			<? if($next != "") {?>
				<a href="<?= base_url().'main/view_report/'.$next?>" class="next">Next<i class="fa fa-arrow-right" style="margin-left:5px;"></i></a>
			<?}?>
		</div>
	</div>
	<?}?>
</div>

<?= $footer?>