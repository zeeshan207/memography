<?= $header?>

<?= $head?>
<?= $left_side?>

<aside class="right-side">
	<!-- Content Header (Page header) -->

	<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-lg-12">
					<section class="panel">
				        <header class="panel-heading">
				      		Filter Data
				      	</header>
				      	<div class="panel-body form-horizontal">
				      		<!-- filter section start here -->
							<div class="row">
								<div class="col-lg-4 form-group">
									<label class="col-lg-4 control-label">Districts</label>
									<div class="col-lg-8">
										<select name="district" class="form-control m-b-10 filter-value">
											<option value="">Select District</option>
										<? foreach($districts as $disc) {?>
											<option value="<?=$disc['district_id']?>"><?=$disc['district_name']?></option>
										<?}?>
										</select>
									</div>
								</div>
								<div class="col-lg-4 form-group">
									<label for="name" class="col-lg-4 col-sm-3 control-label">Age</label>
				                  	<div class="col-lg-8">
				                      	<select name="age" class="form-control m-b-10 filter-value">
				                      		<option value="">Select Age</option>
				                      	<? for ($i = 18; $i <= 70; $i++){ ?>
				                      		<option value="<?=$i?>"><?=$i?></option>
					                    <?}?>
				                      	</select>
				                  	</div>
								</div>
								<div class="col-lg-4 form-group">
									<label for="name" class="col-lg-4 col-sm-3 control-label">Child</label>
				                  	<div class="col-lg-8">
				                      	<select name="child" class="form-control m-b-10 filter-value">
				                      		<option value="">Select No Of Child</option>
				                      	<? for ($i = 1; $i <= 10; $i++){ ?>
				                      		<option value="<?=$i?>"><?=$i?></option>
					                    <?}?>
				                      	</select>
				                  	</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-4 form-group">
									<label for="name" class="col-lg-4 col-sm-3 control-label">BMI</label>
				                  	<div class="col-lg-8">
				                      	<select name="bmi" class="form-control m-b-10 filter-value">
				                      		<option value="">Select BMI</option>
				                      	<? for ($i = 18; $i <= 36; $i++){ ?>
				                      		<option value="<?=$i?>"><?=$i?></option>
					                    <?}?>
				                      	</select>
				                  	</div>
								</div>
								<div class="col-lg-4 form-group">
									<label for="name" class="col-lg-4 col-sm-3 control-label">LMP Date</label>
				                  	<div class="col-lg-8">
				                      	<select name="lmp_date" class="form-control m-b-10 filter-value">
				                      		<option value="">Select LMP Date Ago</option>
				                      		<option value="1">1 Month Ago</option>
				                      		<option value="2">2 Month Ago</option>
				                      		<option value="3">3 Month Ago</option>
				                      		<option value="4">4 Month Ago</option>
				                      		<option value="5">5 Month Ago</option>
				                      		<option value="6">6 Month Ago</option>
				                      		<option value="7">7 Month Ago</option>
				                      		<option value="8">8 Month Ago</option>
				                      		<option value="9">9 Month Ago</option>
				                      		<option value="10">10 Month Ago</option>
				                      		<option value="11">11 Month Ago</option>
				                      		<option value="12">1 Year Ago</option>
				                      	</select>
				                  	</div>
								</div>
								<div class="col-lg-4 form-group">
									<label for="name" class="col-lg-4 col-sm-3 control-label">Outcome</label>
				                  	<div class="col-lg-8">
				                      	<select name="out_come" class="form-control m-b-10 filter-value">
				                      		<option value="">Select Outcome</option>
				                  			<option value="Normal Mammography" >Normal Mammography</option>
				                  			<option value="DCIS"  >DCIS</option>
				                  			<option value="LCIS"  >LCIS</option>
				                  			<option value="DCIS + LCIS" >DCIS + LCIS</option>
				                  			<option value="ILC" >ILC</option>
				                  			<option value="IDC" >IDC</option>
				                  			<option value="IBC" >IBC</option>
				                  		</select>
				                  	</div>
								</div>
							</div>
							<div class="row">
					      		<div class="form-group">
						            <div class="col-lg-offset-10 col-lg-2">
						                <button  class="filter btn btn-info">Filter</button>
						            </div>
						        </div>
					        </div>
							<!-- filter section end  -->
				      	</div>
				    </section>
				</div>
			</div>
		</section>

		<section class="content">
			<div class="row">
				<div class="col-md-2 col-md-offset-10" style="margin-bottom:15px;">
					<a class="btn download btn-info">
						<?php if(count($reports) > 0){?>
							Download All Files
						<?}else{?>
							Records Not Found
						<?}?>
					</a>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<table class="table">
						<thead>
							<tr role='row'>
								<th class="col-lg-1">Sr. No.</th>
								<th class="col-lg-4">Mr No</th>
								<th class="col-lg-3">Name</th>
								<th class="col-lg-2">Status</th>
								<th class="col-lg-1">Action</th>
								<th class="col-lg-1">File</th>
							</tr>
						</thead>
						<tbody id="filtered">
						<? if ( count($reports) ) { 
							$count = $this->uri->segment(3, 0);
						 	foreach($reports as $report) { ?>
							<tr>
								<td class="col-lg-1">
									<?= ++$count ?>
								</td>
								<td class="col-lg-2">
									<?= $report['mr_no']; ?>
								</td>
								<td class="col-lg-5">
									<?= $report['name']; ?>
								</td>
								<td class="col-lg-1">
									<?= anchor("main/deactive_report/".$report['rf_id'],'Deactive',['class'=>'btn btn-danger']);?>
								</td>
								<td class="col-lg-1">
										<?= anchor("main/edit_report/".$report['rf_id'],'Edit',['class'=>'btn btn-primary']);?>
								</td>
								<td class="col-lg-2">
										<?php if($report['doc_file']){?>
										<a class="btn btn-info" download="" href="<?=base_url()."assets/uploads/files/".$report['doc_file']?>">Download</a>
										<?}else{?>
											<label>Records Not Found</label>
										<?}?>
									</div>
								</td>
							</tr>
							<?	} ?>
						<? }else {?>
							<tr>
								<td colspan="5" style=" text-align: center; ">
									No Records Found.
								</td>
							</tr>
							<? } ?>
						</tbody>
					</table>
				</div>
			</div>
		</section>
	</aside>
</div>

<script type="text/javascript">
	
	$(document).ready(function(){
		<?php if(count($reports) > 0){?>
			$('a.download').click(function(e) {
			    e.preventDefault();

			   	<?php foreach($reports as $rep){?>
			    	window.open("<?=base_url('assets/uploads/files/'.$rep['doc_file'])?>");

			    <?}?>
			});
		<?}?>


		// <?php if(count($reports) > 0){?>
		// 	$('a.download').click(function(e) {
		// 	    e.preventDefault();
			    	
		// 	    	var files;
		// 	    	var i;
		// 	   	<?php foreach($reports as $rep){?>
		// 	    	files[i++] = ("<?=base_url('assets/uploads/files/'.$rep['doc_file'])?>");

		// 	    <?}?>

		// 	    alert ('Hello');
		// 	});
		// <?}?>

		$('.filter').attr('disabled', 'disabled');

		$('.filter-value').change(function(){

			var check = $(this).val();

			if(check != ""){

				$('.filter').attr('disabled', false);

			}else{

				$('.filter').attr('disabled', true);				

			}

		});
				

		$('.filter').on('click', function(){
			var total = [];

			var dis = $(this).closest('.panel-body').find('select[name=district]').val();
			var age = $(this).closest('.panel-body').find('select[name=age]').val();
			var child = $(this).closest('.panel-body').find('select[name=child]').val();
			var bmi = $(this).closest('.panel-body').find('select[name=bmi]').val();
			var lmp_date = $(this).closest('.panel-body').find('select[name=lmp_date]').val();
			var out_come = $(this).closest('.panel-body').find('select[name=out_come]').val();

			var get_date = "";

			if(lmp_date != ""){

				var d = new Date();
				var month = d.getMonth()+1;
				var ago = month - lmp_date;
				var day = d.getDate();

				get_date = d.getFullYear() + '/' +
				    (month<10 ? '0' : '') + ago + '/' +
				    (day<10 ? '0' : '') + day;

			}
			
			$.ajax({
            type: "POST",
            url: "<?=base_url()?>main/filter",
            data: {
              'district_idFK': dis,
              'age': age,
              'child': child,
              'bmi': bmi,
              'lmp_date': get_date,
              'out_come': out_come
            },
            dataType: ''
            }).success(function(res) {

            	$('#filtered').html(res);

            });
		});

	});

</script>

<?= $footer?>