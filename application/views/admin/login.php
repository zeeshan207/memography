<?= $header?>

<div class="container">
	<div class="row">
		<div class="col-lg-6 col-lg-offset-3">
			<section class="panel">
		      <header class="panel-heading">
		          Admin & User Login Here
		      </header>
		      <div class="panel-body">
		          <!-- <form class="form-horizontal" role="form"> -->
		          <?= form_open('main/admin_login',['class' => 'form-horizontal', 'role' => 'form']);?>
		              <div class="form-group">
		                  <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">Email</label>
		                  <div class="col-lg-10">
		                      <input type="email" class="form-control" name="email" id="inputEmail1" placeholder="Email" required>
		                  </div>
		              </div>
		              <div class="form-group">
		                  <label for="inputPassword1" class="col-lg-2 col-sm-2 control-label">Password</label>
		                  <div class="col-lg-10">
		                      <input type="password" class="form-control" name="password" id="inputPassword1" placeholder="Password" required>
		                  </div>
		              </div>
		              <div class="form-group">
		                  <div class="col-lg-offset-2 col-lg-10">
		                      <button type="submit" class="btn btn-danger">Sign in</button>
		                  </div>
		              </div>
		          </form>
		      </div>
		  </section>
	  </div>
  </div>
</div>


<?= $footer?>