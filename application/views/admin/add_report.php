<?= $header?>

<?= $head?>
<?= $left_side?>

	<style type="text/css">
		#progress-wrp {
		    border: 1px solid #0099CC;
		    padding: 1px;
		    position: relative;
		    border-radius: 3px;
		    margin: 10px;
		    text-align: left;
		    background: #fff;
		    box-shadow: inset 1px 3px 6px rgba(0, 0, 0, 0.12);
		    height: 26px;
		}
		#progress-wrp .progress-bar{
		    height: 22px;
		    border-radius: 3px;
		    background-color: #7ce150;
		    width: 0;
		    box-shadow: inset 1px 1px 10px rgba(0, 0, 0, 0.11);
		}
		#progress-wrp .status{
		    top:3px;
		    left:50%;
		    position:absolute;
		    display:inline-block;
		    color: #000000;
}
	</style>


<aside class="right-side">
	<!-- Content Header (Page header) -->


	<!-- Main content -->
		<form action="<?if($is_edit){ echo base_url().'main/update_report/'.$id;}else{ echo base_url().'main/submit_report';}?>" class="form-horizontal" method="post">
			<?php echo form_hidden('user_id',$this->session->userdata('user_id')); ?>
		<?php if($this->session->userdata('role') == 'CCH' || $this->session->userdata('role') == 'admin' || $this->session->userdata('role') == 'MOBILE_VAN'){?>
			<section class="content">
				<div class="row">
					<div class="col-lg-12">
						<section class="panel">
					      <header class="panel-heading">
					         <?if($is_edit){
					         	echo 'Update Report';
					         	}else{
					         	echo "Add Report";
					         		} ?>
					      </header>
					      <div class="panel-body">

					          <div class="row">
					          	<div class="col-lg-6">
					          		<div class="form-group">
					                  <label for="mar_no" class="col-lg-3 col-sm-3 control-label">MR No.</label>
					                  <div class="col-lg-6">
					                  	<? if($is_edit){ ?>
											<input type="text" value="<?if(isset($report[$id]['mr_no'])) echo $report[$id]['mr_no']; ?>" class="form-control"  name="mr_no"  placeholder="MR No" >
					                  	<?}else{?>
					                      	<input type="text" class="form-control" name="mr_no" id="mr_no" placeholder="MR No" >
					                  	<?}?>
					                  </div><div class="col-lg-3"> <?php  echo form_error('mr_no'); ?></div>
								  </div>
					          	</div>
								<div class="col-lg-6">
					          		<div class="form-group">
					                  <label for="date" class="col-lg-3 col-sm-3 control-label">Date</label>
					                  <div class="col-lg-3">
					                  	<? if($is_edit){ ?>
					                  		<input type="text" value="<?if(isset($report[$id]['date'])) echo $report[$id]['date']; ?>" class="form-control" readonly name="date"  placeholder="Date" >
					                  	<?}else{?>
					                      	<input type="text" class="form-control" name="date" id="date" placeholder="Date" >
					                  	<?}?>
					                  </div>
					              </div>
					          	</div>
					          </div>
					          <div class="row">
					          	<div class="col-lg-6">
					          		<div class="form-group">
					                  <label for="name" class="col-lg-3 col-sm-3 control-label">Name</label>
					                  <div class="col-lg-9">
					                      <input type="text" class="form-control" name="name" id="name" placeholder="name" value="<?if(isset($report[$id]['name'])) echo $report[$id]['name']; ?>" >
					                  </div>
					              </div>
					          	</div>
					          	<div class="col-lg-6">
					          		<div class="form-group">
					                  <label for="surname" class="col-lg-3 col-sm-3 control-label">W/O D/O</label>
					                  <div class="col-lg-9">
					                      <input type="text" value="<?if(isset($report[$id]['surname'])) echo $report[$id]['surname']; ?>" class="form-control" name="surname" id="surname" placeholder="Surname" >
					                  </div>
					              </div>
					          	</div>
					          </div>
					          <div class="row">
					          	<div class="col-lg-6">
					          		<div class="form-group">
					                  <label for="name" class="col-lg-3 col-sm-3 control-label">Age</label>
					                  <div class="col-lg-3">
					                      <select name="age" id="age" class="form-control m-b-10" >
					                      	<option value="">Please Select</option>
					                      <? for ($i = 18; $i <= 99; $i++){ ?>
					                      		<option value="<?=$i?>" <? if(isset($report[$id]['age'])){ if($report[$id]['age'] == $i) echo 'selected'; } ?> ><?=$i?></option>
						                    <?}?>
					                      </select>
					                  </div>
					              </div>
					          	</div>
					          	<div class="col-lg-6">
				          			<div class="form-group">
					                  <label for="address" class="col-lg-3 col-sm-3 control-label">Postal Address</label>
					                  <div class="col-lg-9">
					                      <input type="text" value="<?if(isset($report[$id]['address'])) echo $report[$id]['address']; ?>" class="form-control" name="address" id="address" placeholder="Postal Address" >
					                  </div>
					                </div>
				          		</div>
					          </div>
					          	<div class="row">
					          		<div class="col-lg-6">
					          			<div class="form-group">
						                  <label for="distric" class="col-lg-3 col-sm-3 control-label">District</label>
						                  <div class="col-lg-9">
						                      <select id="district"  name="district" class="form-control m-b-10" >
						                      	<option value="">Please Select</option>
						                      	<? foreach ($districts as $district) { ?>
						                      		<option value="<?=$district['district_id']?>" <? if(isset($report[$id]['district_id'])){ if($report[$id]['district_id'] == $district['district_id']) echo 'selected'; } ?> ><?=$district['district_name'];?></option>
						                      	<?}?>
						                      </select>
						                  </div>
				          				</div>
				          			</div>
					          		<div class="col-lg-6">
					          			<div class="form-group">
						                  <label for="tehsil" class="col-lg-3 col-sm-3 control-label">Tehsil</label>
						                  <div class="col-lg-9">
						                      <select name="tehsil" id="tehsil" class="form-control m-b-10" >
						                      	<option value="<?if(isset($report[$id]['tehsil_id'])) echo $report[$id]['tehsil_id']; ?>"><?if(isset($report[$id]['tehsil_name'])) echo $report[$id]['tehsil_name']; ?></option>
						                      </select>
						                  </div>
					              		</div>
					              	</div>
				          		</div>
				          		<div class="row">
				          			<div class="col-lg-6">
				          				<div class="form-group">
						                  <label for="name" class="col-lg-3 col-sm-3 control-label">Contact Number</label>
						                  <div class="col-lg-9">
						                      <input value="<?if(isset($report[$id]['f_number'])) echo $report[$id]['f_number']; ?>" class="form-control" id="phoneNumber" placeholder="923001234567" type="tel"  value="92-" title="923001234567" pattern="^[0-9]{12,12}$" name="fnumber" >
						                  </div>
						                </div>
				          			</div>
				          			<div class="col-lg-6">
				          				<div class="form-group">
						                  <label for="name" class="col-lg-3 col-sm-3 control-label">CNIC#</label>
						                  <div class="col-lg-9">
						                      <input value="<?if(isset($report[$id]['h_number'])) echo $report[$id]['h_number']; ?>" class="form-control" value="92-" id="homeNumber" placeholder="00000-0000000-0" type="tel"  title="00000-0000000-0" name="hnumber">
						                  </div>
						                </div>
				          			</div>
				          		</div>
				          		<div class="row">
				          			<div class="col-lg-6">
				          				<div class="form-group">
						                  <label for="name" class="col-lg-3 col-sm-3 control-label">Status</label>
						                  <div class="col-lg-8 col-lg-offset-1">
						                  	<label class="radio-inline">
		                                        <input type="radio" <? if(isset($report[$id]['status'])){ if($report[$id]['status'] == 'single') echo 'checked'; } ?> name="status" id="signle" value="single" > Single
		                                    </label>
		                                    <label class="radio-inline">
		                                        <input <? if(isset($report[$id]['status'])){ if($report[$id]['status'] == 'married') echo 'checked'; } ?> type="radio" name="status" id="married" value="married"> Married
		                                    </label>
						                  </div>
						                </div>
				          			</div>
				          			<? if($is_edit){
				          				 if($report[$id]['child'] != "" ){
					          				$display = 'style="display: block;"';
					          				}
				          			}else{
				          				$display = 'style="display: none;"';
				          			} ?>

				          			<div class="col-lg-6" id="noofchild" <?=$display;?> >
				          				<div class="form-group">
						                  <label for="name" class="col-lg-3 col-sm-3 control-label">No of Children</label>
						                  <div class="col-lg-9">
					          				<select name="child" class="form-control m-b-10">
					          					<option value="">Please Select</option>
					          					<? for ($i = 0; $i <= 15; $i++){ ?>
						                      		<option value="<?=$i?>" <? if(isset($report[$id]['child'])){ if($report[$id]['child'] == $i) echo 'selected'; } ?> ><?=$i?></option>
						                      	<?}?>
						                      </select>
						                   </div>
						                </div>
				          			</div>
				          		</div>
				          		<div class="row">
				          			<div class="col-lg-6">
				          				<div class="form-group">
						                  <label for="ht" class="col-lg-3 col-sm-3 control-label">Height</label>
						                    <div class="col-lg-9">
							                    <div class="row">
								                  	<div class="col-lg-6">
								                  		<div class="form-group">
										                  <div class="col-lg-12">
										                      <input type="text" value="<?if(isset($height)) echo $height; ?>" class="form-control bmi" name="height" id="height" placeholder="Centimeters" >
										                  </div>
										             	</div>
								                  	</div>
							          				<!-- <div class="col-lg-6">
							          					<div class="form-group">
										                  <label for="inch" class="col-lg-3 col-sm-3 control-label">Inch</label>
										                  <div class="col-lg-9">
										                      <input type="text" value="<?if(isset($height)) echo $height[1]; ?>" class="form-control bmi" name="inch" id="inch" placeholder="Inch" >
										                  </div>
										             	</div>
							          				</div> -->
								                </div>
								            </div>
						                </div>
				          			</div>
				          			<div class="col-lg-6">
				          				<div class="form-group">
						                  <label for="wt" class="col-lg-3 col-sm-3 control-label">Weight</label>
						                  <div class="col-lg-9">
					          				<input type="text" value="<?if(isset($report[$id]['weight'])) echo $report[$id]['weight']; ?>" class="form-control bmi" name="weight" id="wt" value="" placeholder="Weight with Kg" >
						                   </div>
						                </div>
				          			</div>
				          		</div>
				          		<div class="row">
				          			<div class="col-lg-6">
				          				<div class="form-group">
						                  <label for="bmi" class="col-lg-3 col-sm-3 control-label">BMI</label>
						                  <div class="col-lg-9">
					          				<select name="bmi" id="bmi" class="form-control m-b-10" >
					          					<option value="">Please Select</option>
					          				<? for ($i = 10; $i <= 56; $i++){ ?>
						                      		<option value="<?=$i?>" <? if(isset($report[$id]['bmi'])){ if($report[$id]['bmi'] == $i) echo 'selected'; } ?> ><?=$i?></option>
						                      	<?}?>
						                      </select>
						                   </div>
						                </div>
				          			</div>
				          			<div class="col-lg-6">
				          				<div class="form-group">
				          					<label for="density" class="col-lg-3 col-sm-3 control-label">Test</label>
						                  	<div class="col-lg-4 col-lg-offset-1">
							                  	<label class="radio-inline">
			                                        <input type="radio" <? if(isset($report[$id]['test'])){ if($report[$id]['test'] == 'screening') echo 'checked'; } ?> name="test" id="screaning" value="screening" > Screening
			                                    </label>
						                  	</div>
						                  	<div class="col-lg-4">
						                  		<label class="radio-inline">
		                                        	<input type="radio" <? if(isset($report[$id]['test'])){ if($report[$id]['test'] == 'diagnostic') echo 'checked'; } ?> name="test" id="dignostic" value="diagnostic"> Diagnostic
		                                    	</label>
						                  	</div>
						                </div>

				          			</div>
				          		</div>
				          		<div class="row">
						          	<div class="col-lg-6">
						          		<div class="form-group">
						                  	<label for="date" class="col-lg-3 col-sm-3 control-label">LMP Date</label>
							                <div class="col-lg-9">
						                  		<input type="text" id="lmp-date" value="<?if(isset($report[$id]['lmp_date'])) echo $report[$id]['lmp_date']; ?>" class="form-control" name="lmp_date"  placeholder="LMP Date" >
						                  	</div>
						              </div>
						          	</div>
						          	<div class="col-lg-6">
				          				<div class="form-group">
				          					<label for="density" class="col-lg-3 col-sm-3 control-label">Post-Menopausal</label>
						                  	<div class="col-lg-4 col-lg-offset-1">
							                  	<label class="radio-inline">
			                                        <input type="radio" <? if(isset($report[$id]['post_meno'])){ if($report[$id]['post_meno'] == 'Yes') echo 'checked'; } ?> name="post_meno"  value="Yes" > Yes
			                                    </label>
						                  	</div>
						                  	<div class="col-lg-4">
						                  		<label class="radio-inline">
		                                        	<input type="radio" <? if(isset($report[$id]['post_meno'])){ if($report[$id]['post_meno'] == 'No') echo 'checked'; } ?> name="post_meno" value="No"> No
		                                    	</label>
						                  	</div>
						                </div>
				          			</div>
					          	</div>
					          	<div class="row">
					          		<div class="col-lg-6">
				          				<div class="form-group">
				          					<label for="density" class="col-lg-3 col-sm-3 control-label">Pre-Menopausal</label>
						                  	<div class="col-lg-4 col-lg-offset-1">
							                  	<label class="radio-inline">
			                                        <input type="radio" <? if(isset($report[$id]['pre_meno'])){ if($report[$id]['pre_meno'] == 'Yes') echo 'checked'; } ?> name="pre_meno"  value="Yes" > Yes
			                                    </label>
						                  	</div>
						                  	<div class="col-lg-4">
						                  		<label class="radio-inline">
		                                        	<input type="radio" <? if(isset($report[$id]['pre_meno'])){ if($report[$id]['pre_meno'] == 'No') echo 'checked'; } ?> name="pre_meno" value="No"> No
		                                    	</label>
						                  	</div>
						                </div>
				          			</div>
					          		<div class="col-lg-6">
				          				<div class="form-group">
				          					<label for="density" class="col-lg-3 col-sm-3 control-label">Pregnancy</label>
						                  	<div class="col-lg-4 col-lg-offset-1">
							                  	<label class="radio-inline">
			                                        <input type="radio" <? if(isset($report[$id]['pregnancy'])){ if($report[$id]['pregnancy'] == 'Yes') echo 'checked'; } ?> name="pregnancy"  value="Yes" > Yes
			                                    </label>
						                  	</div>
						                  	<div class="col-lg-4">
						                  		<label class="radio-inline">
		                                        	<input type="radio" <? if(isset($report[$id]['pregnancy'])){ if($report[$id]['pregnancy'] == 'No') echo 'checked'; } ?> name="pregnancy" value="No"> No
		                                    	</label>
						                  	</div>
						                </div>
				          			</div>
					          	</div>
					      	</div>
					  	</section>
				  	</div>
			  	</div>
			</section>

			<section class="content">
				<div class="row">
					<div class="col-lg-12">
						<section class="panel">
					      	<header class="panel-heading">
					          Recommendation
					      	</header>
					      	<div class="panel-body">
					      		<div class="row">
					      			<div class="col-lg-6">
						          		<div class="form-group">
						                    <label for="sc_date" class="col-lg-3 col-sm-3 control-label">Screening Date</label>
						                    <div class="col-lg-9">
						                    	<div class="input-group m-b-10">
							                    	<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							                       <input value="<?if(isset($report[$id]['sc_date'])) echo $report[$id]['sc_date']; ?>" type="text" class="form-control" autocomplete="off" name="sc_date" id="sc-date" placeholder="Screening Date" >
						                       </div>
						                    </div>
						                </div>
					          		</div>
					          		<div class="col-lg-6">
						          		<div class="form-group">
						                    <label for="inves" class="col-lg-3 col-sm-3 control-label">Investigation</label>
						                    <div class="col-lg-9">
						                    	<div class="col-lg-6">
						                  		<label class="radio-inline">
		                                        	<input <?if(isset($report[$id]['investi']) == "Recall imaging") echo "checked"; ?> type="radio" name="investi" id="investi" value="Recall imaging" > Recall imaging
		                                    	</label>
							                  	</div>
							                  	<div class="col-lg-6">
							                  		<label class="radio-inline">
			                                        	<input <?if(isset($report[$id]['investi']) == "call for Biopsy") echo "checked"; ?> type="radio" name="investi" id="investi" value="call for Biopsy"> Call for Biopsy
			                                    	</label>
							                  	</div>
						                    </div>
						                </div>
					          		</div>
					      		</div>
					      		<div class="row">
					      			<div class="col-lg-6" >
					      				<div id="file-upload">
					      					<label class="control-label col-lg-3">Upload File</label>
						      				<div class="col-lg-4">
						      					<input type="file" accept=".doc, .docx, .zip"  id="file">
						      					<label class="control-label">Allowed doc, docx, zip</label>
						      				</div>
					      				</div>
					      				<div id="file-show">
					      					<? if($is_edit) {?>
							      				<div style="margin-left:157px;float:left;" class="date-blob">
						      						<?if(isset($report[$id]['doc_file'])) echo $report[$id]['doc_file']; ?>
						      						<span class="date-enable"><i style="margin-left:10px;cursor: pointer;" class="fa fa-times"></i></span>
						      						<input type="hidden" size="20"  name="doc_file" value="<?if(isset($report[$id]['doc_file'])) echo $report[$id]['doc_file']; ?>">
						      						<label class="control-label">Allowed doc, docx, zip</label>
						      					</div>
					      					<?}?>
					      				</div>
					      			</div>
					      			<div class="col-lg-4">
				      					<div id="progress-wrp"><div class="progress-bar"></div ><div class="status">0%</div></div>
										<div id="output"><!-- error or success results --></div>
					      			</div>
					      		</div>
					     	</div>
					    </section>
				    </div>
				</div>
			</section>
			<?if($is_edit){?>
			<section class="content">
				<div class="row">
					<div class="col-lg-12">
						<section class="panel">
					      	<header class="panel-heading">Image Upload</header>
					      	<div class="panel-body">
					      		<div class="row" style="margin-bottom: 8px;">
				      				<div class="col-lg-2">
						                <input type="file" size="20" name="img" id="img">
				      				</div>
				      			</div>
				      			<div class="row" id="imagas">
				      				<? if($images == true){?>
					      				<? foreach ($images as $img) {?>
					      					<div class="col-lg-3">
					      						<input type="hidden" name="img[]" value="<?=$img['image_name'];?>">
					      						<button id="img-close" class="close" type="button" style="color: red" data-name="<?=$img['image_name'];?>" >x</button>
					      						<div class="profile-img" style="background-image: url(<?=base_url()."assets/uploads/images/".$img['image_name'];?>);">
					      							
					      						</div>
					      					</div>
					      				<? } ?>
				      				<?}?>
				      			</div>
					      	</div>
					    </section>
					</div>
				</div>
			</section>
			<?}?>
		<?}?>

		<?php if($this->session->userdata('role') == 'ALRAZI'){?>
			<!-- out come -->
			<section class="content">
				<div class="row">
					<div class="col-lg-12">
						<section class="panel">
					        <header class="panel-heading">
					            Outcome
					        </header>
					        <div class="panel-body">
					      		<div class="row">
					          	<div class="col-lg-6">
					          		<div class="form-group">
					                    <label for="date" class="col-lg-3 col-sm-3 control-label">Outcome</label>
					                    <div class="col-lg-9">
					                  		<select name="out_come" class="form-control m-b-10" ="">
					                  			<option value="">Please Select</option>
					                  			<option value="Normal Mammography" <? if(isset($report[$id]['out_come'])){ if($report[$id]['out_come'] == "Normal Mammography") echo 'selected'; } ?> >Normal Mammography</option>
					                  			<option value="DCIS" <? if(isset($report[$id]['out_come'])){ if($report[$id]['out_come'] == "DCIS") echo 'selected'; } ?> >DCIS</option>
					                  			<option value="LCIS" <? if(isset($report[$id]['out_come'])){ if($report[$id]['out_come'] == "LCIS") echo 'selected'; } ?> >LCIS</option>
					                  			<option value="DCIS + LCIS" <? if(isset($report[$id]['out_come'])){ if($report[$id]['out_come'] == "DCIS + LCIS") echo 'selected'; } ?> >DCIS + LCIS</option>
					                  			<option value="ILC" <? if(isset($report[$id]['out_come'])){ if($report[$id]['out_come'] == "ILC") echo 'selected'; } ?> >ILC</option>
					                  			<option value="IDC" <? if(isset($report[$id]['out_come'])){ if($report[$id]['out_come'] == "IDC") echo 'selected'; } ?> >IDC</option>
					                  			<option value="IBC" <? if(isset($report[$id]['out_come'])){ if($report[$id]['out_come'] == "IBC") echo 'selected'; } ?> >IBC</option>
					                  		</select>
					                    </div>
					              </div>
					          	</div>
					          </div>
					        </div>
					    </section>
					</div>
				</div>
			</section>
			<!-- left chest -->
			<section class="content">
				<div class="row">
					<div class="col-lg-12">
						<section class="panel">
					      <header class="panel-heading">
					          Findings Left Breast
					      </header>
					      <div class="panel-body">
					      	<input type="hidden" name="fd_id[]" value="<?= $report[$id]['fd_details'][0]['fd_id'];?>">
					      	<div class="row">
			          			<div class="col-lg-6">
			          				<div class="form-group">
			          					<label for="density" class="col-lg-4 col-sm-3 control-label">Density</label>
			          					<div class="col-lg-8">
						                  	<select name="density[]" class="form-control m-b-10" ="">
						                  		<option value="">Please Select</option>
					                  			<option value="Fatty" <? if(isset($report[$id]['fd_details'][0]['density'])){ if($report[$id]['fd_details'][0]['density'] == "Fatty") echo 'selected'; } ?> >Fatty</option>
					                  			<option value="Mildly Dense" <? if(isset($report[$id]['fd_details'][0]['density'])){ if($report[$id]['fd_details'][0]['density'] == "Mildly Dense") echo 'selected'; } ?> >Mildly Dense</option>
					                  			<option value="Moderately Dense" <? if(isset($report[$id]['fd_details'][0]['density'])){ if($report[$id]['fd_details'][0]['density'] == "Moderately Dense") echo 'selected'; } ?> >Moderately Dense</option>
					                  			<option value="Only Dense" <? if(isset($report[$id]['fd_details'][0]['density'])){ if($report[$id]['fd_details'][0]['density'] == "Only Dense") echo 'selected'; } ?> >Only Dense</option>
					                  		</select>
				                  		</div>
					                </div>
			          			</div>
			          			<div class="col-lg-6">
					            		<div class="form-group" >
				          					<label for="density" class="col-lg-5 col-sm-3 control-label">BI RAD Category</label>
				          					<div class="col-lg-7">
							                  	<select name="birad[]" class="form-control m-b-10" >
							                  		<option value="">Please Select</option>
							                      	<option value="0" <? if(isset($report[$id]['fd_details'][0]['birad'])){ if($report[$id]['fd_details'][0]['birad'] == "0") echo 'selected'; } ?>>0</option>
							                      	<option value="1" <? if(isset($report[$id]['fd_details'][0]['birad'])){ if($report[$id]['fd_details'][0]['birad'] == "1") echo 'selected'; } ?>>1</option>
							                      	<option value="2" <? if(isset($report[$id]['fd_details'][0]['birad'])){ if($report[$id]['fd_details'][0]['birad'] == "2") echo 'selected'; } ?>>2</option>
							                      	<option value="3" <? if(isset($report[$id]['fd_details'][0]['birad'])){ if($report[$id]['fd_details'][0]['birad'] == "3") echo 'selected'; } ?>>3</option>
							                      	<option value="4a" <? if(isset($report[$id]['fd_details'][0]['birad'])){ if($report[$id]['fd_details'][0]['birad'] == "4a") echo 'selected'; } ?>>4a</option>
							                      	<option value="4b" <? if(isset($report[$id]['fd_details'][0]['birad'])){ if($report[$id]['fd_details'][0]['birad'] == "4b") echo 'selected'; } ?>>4b</option>
							                      	<option value="4c" <? if(isset($report[$id]['fd_details'][0]['birad'])){ if($report[$id]['fd_details'][0]['birad'] == "4c") echo 'selected'; } ?>>4c</option>
							                      	<option value="5" <? if(isset($report[$id]['fd_details'][0]['birad'])){ if($report[$id]['fd_details'][0]['birad'] == "5") echo 'selected'; } ?>>5</option>
							                      	<option value="6" <? if(isset($report[$id]['fd_details'][0]['birad'])){ if($report[$id]['fd_details'][0]['birad'] == "6") echo 'selected'; } ?>>6</option>
							                    </select>
							                  </div>
						                </div>
					            	</div>
			          		</div>
					      	<div class="row">
					            	<div class="col-lg-6">
					            		<div class="form-group">
						                  <label for="ht" class="col-lg-4 col-sm-3 control-label" style="text-align: right;">Spiculated Focal Mass</label>
						                    <div class="col-lg-8">
							                    <div class="row">
								                  	<div class="col-lg-5">
								                  		<div class="form-group">
										                  <div class="col-lg-12">
										                  	<select name="sf_mass1[]" class="form-control m-b-10" >
										                  		<option value="">Please Select</option>
										                    <? for ($i = 1; $i <= 10; $i++){ ?>
									                      		<option value="<?=$i?>" <? if(isset($report[$id]['fd_details'][0]['focal_mass'][0])){ if($report[$id]['fd_details'][0]['focal_mass'][0] == $i) echo 'selected'; } ?> ><?=$i?></option>
									                      	<?}?>
										                    </select>
										                  </div>
										             	</div>
								                  	</div>
								                  	<div class="col-lg-2" style="text-align: center;">
								                  		<span>X</span>
								                  	</div>
							          				<div class="col-lg-5">
							          					<div class="form-group">
										                  <div class="col-lg-12">
										                  	<select name="sf_mass2[]" class="form-control m-b-10" >
										                  		<option value="">Please Select</option>
										                  	<? for ($i = 1; $i <= 10; $i++){ ?>
									                      		<option value="<?=$i?>" <? if(isset($report[$id]['fd_details'][0]['focal_mass'][1])){ if($report[$id]['fd_details'][0]['focal_mass'][1] == $i) echo 'selected'; } ?> ><?=$i?></option>
									                      	<?}?>
										                    </select>
										                  </div>
										             	</div>
							          				</div>
								                </div>
								            </div>
						                </div>
					            	</div>
					            	<div class="col-lg-6">
					            		<div class="form-group">
				          					<label for="density" class="col-lg-5 col-sm-3 control-label" >Clustered Micro Calcification</label>
							                  <div class="col-lg-7">
							                  	<select name="cmcal[]" class="form-control m-b-10" >
							                  		<option value="">Please Select</option>
							                      	<option value="yes" <? if(isset($report[$id]['fd_details'][0]['cm_cal'])){ if($report[$id]['fd_details'][0]['cm_cal'] == "yes") echo 'selected'; } ?> >Yes</option>
							                      	<option value="no" <? if(isset($report[$id]['fd_details'][0]['cm_cal'])){ if($report[$id]['fd_details'][0]['cm_cal'] == "no") echo 'selected'; } ?>>No</option>
							                    </select>
							                  </div>
						                </div>
					            	</div>
					            </div>
					            <div class="row">
					            	<div class="col-lg-6">
					            		<div class="form-group">
				          					<label for="density" class="col-lg-4 col-sm-3 control-label" >Architectural Distortion</label>
							                  <div class="col-lg-8">
							                  	<select name="adis[]" class="form-control m-b-10" >
							                  		<option value="">Please Select</option>
							                  		<option value="yes" <? if(isset($report[$id]['fd_details'][0]['a_dis'])){ if($report[$id]['fd_details'][0]['a_dis'] == "yes") echo 'selected'; } ?> >Yes</option>
							                      	<option value="no" <? if(isset($report[$id]['fd_details'][0]['a_dis'])){ if($report[$id]['fd_details'][0]['a_dis'] == "no") echo 'selected'; } ?>>No</option>
							                    </select>
							                  </div>
						                </div>
						            </div>
						            <div class="col-lg-6">
					            		<div class="form-group">
				          					<label for="density" class="col-lg-5 col-sm-3 control-label" >Linear Branching Micro Calcifications</label>
							                  <div class="col-lg-7">
							                  	<select name="lbmcal[]" class="form-control m-b-10" >
							                  		<option value="">Please Select</option>
							                  		<option value="yes" <? if(isset($report[$id]['fd_details'][0]['lbm_cal'])){ if($report[$id]['fd_details'][0]['lbm_cal'] == "yes") echo 'selected'; } ?> >Yes</option>
							                      	<option value="no" <? if(isset($report[$id]['fd_details'][0]['lbm_cal'])){ if($report[$id]['fd_details'][0]['lbm_cal'] == "no") echo 'selected'; } ?>>No</option>
							                    </select>
							                  </div>
						                </div>
						            </div>
					            </div>
					            <div class="row">
					            	<div class="col-lg-6">
					            		<div class="form-group" >
				          					<label for="density"  class="col-lg-4 col-sm-3 control-label">Asymmetry</label>
				          					<div class="col-lg-8">
							                  	<select name="asymmetry[]" class="form-control m-b-10" >
							                  		<option value="">Please Select</option>
							                      	<option value="yes" <? if(isset($report[$id]['fd_details'][0]['asymmetry'])){ if($report[$id]['fd_details'][0]['asymmetry'] == "yes") echo 'selected'; } ?> >Yes</option>
							                      	<option value="no" <? if(isset($report[$id]['fd_details'][0]['asymmetry'])){ if($report[$id]['fd_details'][0]['asymmetry'] == "no") echo 'selected'; } ?>>No</option>
							                    </select>
							                  </div>
						                </div>
					            	</div>
					            	<div class="col-lg-6">
					            		<div class="form-group" >
				          					<label for="density" class="col-lg-5 col-sm-3 control-label">Quadrant</label>
				          					<div class="col-lg-7">
							                  	<select name="quadrant[]" class="form-control m-b-10" >
							                  		<option value="">Please Select</option>
							                      	<option value="UO" <? if(isset($report[$id]['fd_details'][0]['quadrant'])){ if($report[$id]['fd_details'][0]['quadrant'] == "UO") echo 'selected'; } ?>>UO</option>
							                      	<option value="UI" <? if(isset($report[$id]['fd_details'][0]['quadrant'])){ if($report[$id]['fd_details'][0]['quadrant'] == "UI") echo 'selected'; } ?>>UI</option>
							                      	<option value="LU" <? if(isset($report[$id]['fd_details'][0]['quadrant'])){ if($report[$id]['fd_details'][0]['quadrant'] == "LU") echo 'selected'; } ?>>LU</option>
							                      	<option value="LI" <? if(isset($report[$id]['fd_details'][0]['quadrant'])){ if($report[$id]['fd_details'][0]['quadrant'] == "LI") echo 'selected'; } ?>>LI</option>
							                      	<option value="LO" <? if(isset($report[$id]['fd_details'][0]['quadrant'])){ if($report[$id]['fd_details'][0]['quadrant'] == "LO") echo 'selected'; } ?>>LO</option>
							                      	<option value="C" <? if(isset($report[$id]['fd_details'][0]['quadrant'])){ if($report[$id]['fd_details'][0]['quadrant'] == "C") echo 'selected'; } ?>>C</option>
							                    </select>
							                  </div>
						                </div>
					            	</div>
					            </div>
					      	</div>
					    </section>
					</div>
				</div>
			</section>
			<!-- for right chest -->
			<section class="content">
				<div class="row">
					<div class="col-lg-12">
						<section class="panel">
					      <header class="panel-heading">
					          Findings Right Breast
					      </header>
					      <div class="panel-body">
					      	<input type="hidden" name="fd_id[]" value="<?= $report[$id]['fd_details'][1]['fd_id'];?>">
					      	<div class="row">
			          			<div class="col-lg-6">
			          				<div class="form-group">
			          					<label for="density" class="col-lg-4 col-sm-3 control-label">Density</label>
			          					<div class="col-lg-8">
						                  	<select name="density[]" class="form-control m-b-10" ="">
						                  		<option value="">Please Select</option>
					                  			<option value="Fatty" <? if(isset($report[$id]['fd_details'][1]['density'])){ if($report[$id]['fd_details'][1]['density'] == "Fatty") echo 'selected'; } ?> >Fatty</option>
					                  			<option value="Mildly Dense" <? if(isset($report[$id]['fd_details'][1]['density'])){ if($report[$id]['fd_details'][1]['density'] == "Mildly Dense") echo 'selected'; } ?> >Mildly Dense</option>
					                  			<option value="Moderately Dense" <? if(isset($report[$id]['fd_details'][1]['density'])){ if($report[$id]['fd_details'][1]['density'] == "Moderately Dense") echo 'selected'; } ?> >Moderately Dense</option>
					                  			<option value="Only Dense" <? if(isset($report[$id]['fd_details'][1]['density'])){ if($report[$id]['fd_details'][1]['density'] == "Only Dense") echo 'selected'; } ?> >Only Dense</option>
					                  		</select>
				                  		</div>
					                </div>
			          			</div>
			          			<div class="col-lg-6">
					            		<div class="form-group" >
				          					<label for="density" class="col-lg-5 col-sm-3 control-label">BI RAD Category</label>
				          					<div class="col-lg-7">
							                  	<select name="birad[]" class="form-control m-b-10" >
							                  		<option value="">Please Select</option>
							                      	<option value="0" <? if(isset($report[$id]['fd_details'][1]['birad'])){ if($report[$id]['fd_details'][1]['birad'] == "0") echo 'selected'; } ?>>0</option>
							                      	<option value="1" <? if(isset($report[$id]['fd_details'][1]['birad'])){ if($report[$id]['fd_details'][1]['birad'] == "1") echo 'selected'; } ?>>1</option>
							                      	<option value="2" <? if(isset($report[$id]['fd_details'][1]['birad'])){ if($report[$id]['fd_details'][1]['birad'] == "2") echo 'selected'; } ?>>2</option>
							                      	<option value="3" <? if(isset($report[$id]['fd_details'][1]['birad'])){ if($report[$id]['fd_details'][1]['birad'] == "3") echo 'selected'; } ?>>3</option>
							                      	<option value="4a" <? if(isset($report[$id]['fd_details'][1]['birad'])){ if($report[$id]['fd_details'][1]['birad'] == "4a") echo 'selected'; } ?>>4a</option>
							                      	<option value="4b" <? if(isset($report[$id]['fd_details'][1]['birad'])){ if($report[$id]['fd_details'][1]['birad'] == "4b") echo 'selected'; } ?>>4b</option>
							                      	<option value="4c" <? if(isset($report[$id]['fd_details'][1]['birad'])){ if($report[$id]['fd_details'][1]['birad'] == "4c") echo 'selected'; } ?>>4c</option>
							                      	<option value="5" <? if(isset($report[$id]['fd_details'][1]['birad'])){ if($report[$id]['fd_details'][1]['birad'] == "5") echo 'selected'; } ?>>5</option>
							                      	<option value="6" <? if(isset($report[$id]['fd_details'][1]['birad'])){ if($report[$id]['fd_details'][1]['birad'] == "6") echo 'selected'; } ?>>6</option>
							                    </select>
							                  </div>
						                </div>
					            	</div>
			          		</div>
					      	<div class="row">
					            	<div class="col-lg-6">
					            		<div class="form-group">
						                  <label for="ht" class="col-lg-4 col-sm-3 control-label" style="text-align: right;">Spiculated Focal Mass</label>
						                    <div class="col-lg-8">
							                    <div class="row">
								                  	<div class="col-lg-5">
								                  		<div class="form-group">
										                  <div class="col-lg-12">
										                  	<select name="sf_mass1[]" class="form-control m-b-10" >
										                  		<option value="">Please Select</option>
										                    <? for ($i = 1; $i <= 10; $i++){ ?>
									                      		<option value="<?=$i?>" <? if(isset($report[$id]['fd_details'][1]['focal_mass'][0])){ if($report[$id]['fd_details'][1]['focal_mass'][0] == $i) echo 'selected'; } ?> ><?=$i?></option>
									                      	<?}?>
										                    </select>
										                  </div>
										             	</div>
								                  	</div>
								                  	<div class="col-lg-2" style="text-align: center;">
								                  		<span>X</span>
								                  	</div>
							          				<div class="col-lg-5">
							          					<div class="form-group">
										                  <div class="col-lg-12">
										                  	<select name="sf_mass2[]" class="form-control m-b-10" >
										                  		<option value="">Please Select</option>
										                  	<? for ($i = 1; $i <= 10; $i++){ ?>
									                      		<option value="<?=$i?>" <? if(isset($report[$id]['fd_details'][1]['focal_mass'][1])){ if($report[$id]['fd_details'][1]['focal_mass'][1] == $i) echo 'selected'; } ?> ><?=$i?></option>
									                      	<?}?>
										                    </select>
										                  </div>
										             	</div>
							          				</div>
								                </div>
								            </div>
						                </div>
					            	</div>
					            	<div class="col-lg-6">
					            		<div class="form-group">
				          					<label for="density" class="col-lg-5 col-sm-3 control-label" >Clustered Micro Calcification</label>
							                  <div class="col-lg-7">
							                  	<select name="cmcal[]" class="form-control m-b-10" >
							                  		<option value="">Please Select</option>
							                      	<option value="yes" <? if(isset($report[$id]['fd_details'][1]['cm_cal'])){ if($report[$id]['fd_details'][1]['cm_cal'] == "yes") echo 'selected'; } ?> >Yes</option>
							                      	<option value="no" <? if(isset($report[$id]['fd_details'][1]['cm_cal'])){ if($report[$id]['fd_details'][1]['cm_cal'] == "no") echo 'selected'; } ?>>No</option>
							                    </select>
							                  </div>
						                </div>
					            	</div>
					            </div>
					            <div class="row">
					            	<div class="col-lg-6">
					            		<div class="form-group">
				          					<label for="density" class="col-lg-4 col-sm-3 control-label" >Architectural Distortion</label>
							                  <div class="col-lg-8">
							                  	<select name="adis[]" class="form-control m-b-10" >
							                  		<option value="">Please Select</option>
							                  		<option value="yes" <? if(isset($report[$id]['fd_details'][1]['a_dis'])){ if($report[$id]['fd_details'][1]['a_dis'] == "yes") echo 'selected'; } ?> >Yes</option>
							                      	<option value="no" <? if(isset($report[$id]['fd_details'][1]['a_dis'])){ if($report[$id]['fd_details'][1]['a_dis'] == "no") echo 'selected'; } ?>>No</option>
							                    </select>
							                  </div>
						                </div>
						            </div>
						            <div class="col-lg-6">
					            		<div class="form-group">
				          					<label for="density" class="col-lg-5 col-sm-3 control-label" >Linear Branching Micro Calcifications</label>
							                  <div class="col-lg-7">
							                  	<select name="lbmcal[]" class="form-control m-b-10" >
							                  		<option value="">Please Select</option>
							                  		<option value="yes" <? if(isset($report[$id]['fd_details'][1]['lbm_cal'])){ if($report[$id]['fd_details'][1]['lbm_cal'] == "yes") echo 'selected'; } ?> >Yes</option>
							                      	<option value="no" <? if(isset($report[$id]['fd_details'][1]['lbm_cal'])){ if($report[$id]['fd_details'][1]['lbm_cal'] == "no") echo 'selected'; } ?>>No</option>
							                    </select>
							                  </div>
						                </div>
						            </div>
					            </div>
					            <div class="row">
					            	<div class="col-lg-6">
					            		<div class="form-group" >
				          					<label for="density"  class="col-lg-4 col-sm-3 control-label">Asymmetry</label>
				          					<div class="col-lg-8">
							                  	<select name="asymmetry[]" class="form-control m-b-10" >
							                  		<option value="">Please Select</option>
							                      	<option value="yes" <? if(isset($report[$id]['fd_details'][1]['asymmetry'])){ if($report[$id]['fd_details'][1]['asymmetry'] == "yes") echo 'selected'; } ?> >Yes</option>
							                      	<option value="no" <? if(isset($report[$id]['fd_details'][1]['asymmetry'])){ if($report[$id]['fd_details'][1]['asymmetry'] == "no") echo 'selected'; } ?>>No</option>
							                    </select>
							                  </div>
						                </div>
					            	</div>
					            	<div class="col-lg-6">
					            		<div class="form-group" >
				          					<label for="density" class="col-lg-5 col-sm-3 control-label">Quadrant</label>
				          					<div class="col-lg-7">
							                  	<select name="quadrant[]" class="form-control m-b-10" >
							                  		<option value="">Please Select</option>
							                      	<option value="UO" <? if(isset($report[$id]['fd_details'][1]['quadrant'])){ if($report[$id]['fd_details'][1]['quadrant'] == "UO") echo 'selected'; } ?>>UO</option>
							                      	<option value="UI" <? if(isset($report[$id]['fd_details'][1]['quadrant'])){ if($report[$id]['fd_details'][1]['quadrant'] == "UI") echo 'selected'; } ?>>UI</option>
							                      	<option value="LU" <? if(isset($report[$id]['fd_details'][1]['quadrant'])){ if($report[$id]['fd_details'][1]['quadrant'] == "LU") echo 'selected'; } ?>>LU</option>
							                      	<option value="LI" <? if(isset($report[$id]['fd_details'][1]['quadrant'])){ if($report[$id]['fd_details'][1]['quadrant'] == "LI") echo 'selected'; } ?>>LI</option>
							                      	<option value="LO" <? if(isset($report[$id]['fd_details'][1]['quadrant'])){ if($report[$id]['fd_details'][1]['quadrant'] == "LO") echo 'selected'; } ?>>LO</option>
							                      	<option value="C" <? if(isset($report[$id]['fd_details'][1]['quadrant'])){ if($report[$id]['fd_details'][1]['quadrant'] == "C") echo 'selected'; } ?>>C</option>
							                    </select>
							                  </div>
						                </div>
					            	</div>
					            </div>
					      	</div>
					    </section>
					</div>
				</div>
			</section>
			<!-- end right chest -->

			<section class="content">
				<div class="row">
					<div class="col-lg-12">
						<section class="panel">
							<header class="panel-heading">BILATRAL REPORT</header>
							<div class="panel-body">
								<input type="hidden" name="fd_id[]" value="<?= $report[$id]['fd_details'][1]['fd_id'];?>">
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group">
											<label for="name" class="col-lg-3 col-sm-3 control-label">Technique</label>
											<div class="col-lg-9">
												<textarea rows="4" value="<?if(isset($report[$id]['technique'])) echo $report[$id]['technique']; ?>" class="form-control" id="tenchnique" placeholder="Technique" name="technique" ></textarea>
											</div>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label for="name" class="col-lg-3 col-sm-3 control-label">Impression</label>
											<div class="col-lg-9">
												<textarea rows="4" value="<?if(isset($report[$id]['r_breast'])) echo $report[$id]['impression']; ?>" class="form-control" id="impression" placeholder="Impression" name="impression"></textarea> 
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group">
											<label for="name" class="col-lg-3 col-sm-3 control-label">Left Breast</label>
											<div class="col-lg-9">
												<textarea rows="4" value="<?if(isset($report[$id]['l_breast'])) echo $report[$id]['l_breast']; ?>" class="form-control" id="l_breast" placeholder="Left Breast" name="l_breast" >
												</textarea>
											</div>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label for="name" class="col-lg-3 col-sm-3 control-label">Right Breast</label>
											<div class="col-lg-9">
												<textarea rows="4" value="<?if(isset($report[$id]['r_breast'])) echo $report[$id]['r_breast']; ?>" class="form-control" id="r_breast" placeholder="Right Breast" name="r_breast">
												</textarea>
											</div>
										</div>
									</div>
								</div>

							</div>
						</section>
					</div>
				</div>
			</section> <!-- end of report-->
		<?}?>
		<div class="row">
      		<div class="form-group">
	            <div class="col-lg-offset-10 col-lg-2">
	                <button type="submit" class="btn btn-info">Submit</button>
	            </div>
	        </div>
        </div>
        </form>
	</aside>
</div>

<script type="text/javascript">
	
	$(document).ready(function(){

		<? if($is_edit) {?>

			<? if($report[$id]['doc_file'] != "") {?>
				
				file_upload_hide();
				file_container_show();

			<?}else{?>
				
				file_upload_show();
				file_container_hide();
			<?}?>

		<?}else{?>

			file_container_hide();

		<?}?>
    	
    // docx file upload section
        $('#file').on('change',function(){


	        var file = $(this).prop("files")[0];
	        var form_data = new FormData();
	        form_data.append("file", file);

            $.ajax({
                data: form_data,
                processData: false,
                contentType: false,
                cache: false,
                url: "<?=base_url()?>main/do_upload",
                type: "POST",
                dataType: "",
                xhr: function(){
			        //upload Progress
			        var xhr = $.ajaxSettings.xhr();
			        if (xhr.upload) {
			            xhr.upload.addEventListener('progress', function(event) {
			                var percent = 0;
			                var position = event.loaded || event.position;
			                var total = event.total;
			                if (event.lengthComputable) {
			                    percent = Math.ceil(position / total * 100);
			                }
			                //update progressbar
			                $(" .progress-bar").css("width", + percent +"%");
			                $(" .status").text(percent +"%");
			            }, true);
			        }
			        return xhr;
			    },
              }).success(function(res) {

                $('#file-show').append(res);
                file_container_show();
                file_upload_hide();

            });

        });

		// bmi calculate
        $('.bmi').on('blur', function(){
            var inch = "";

            var weight = $(this).closest('.panel-body').find('input[name=weight]').val();
            var height = $(this).closest('.panel-body').find('input[name=height]').val();

            if(weight != "" && height != ""){

            	var cm = parseFloat(height) * parseFloat(height);
	            var bmi = weight/cm*10000;
	            var result = parseInt(bmi);

	            $(this).closest('.panel-body').find('select[name=bmi]').val(result);

            }


        });

        // upload file remove option
        $('body').delegate('.date-enable', 'click', function(){
        	
          var name = $(this).closest('.date-blob').text();
          $(this).parent().remove();
          file_upload_show();
          file_container_hide();

          $.ajax({
                url: "<?=base_url()?>main/delete_file/"+name,
                type: "POST",
                dataType: "",
              }).success(function(res) {

              	$(" .progress-bar").css("width", "0px");
			    $(" .status").text(0 +"%");

            });

        });


});

function file_upload_show()
{
  $('#file-upload').show();
}

function file_upload_hide()
{
  $('#file-upload').hide();
}

function file_container_show()
{
  $('#file-show').show();
}

function file_container_hide()
{
  $('#file-show').hide();
}

</script>
<?= $footer?>

