<?= $header?>

<?= $head?>
<?= $left_side?>
	<aside class="right-side">
	<!-- Content Header (Page header) -->


	<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-lg-12">
					<section class="panel">
				      <header class="panel-heading">
				      	<? if($is_edit){
				      		echo "Update User";
				      	}else{
				      		echo "Create User";
				      	}?>
				      </header>

				      <div class="panel-body">
				          <form action="<?if($is_edit){ echo base_url().'main/update_user/'.$user[0]['user_id'].'';}else{ echo base_url().'main/register';}?>" class="form-horizontal" method="post">
				          	<div class="form-group">
				                  <label for="inputfirstname" class="col-lg-2 col-sm-2 control-label">First Name</label>
				                  <div class="col-lg-6">
				                      <input type="text" class="form-control" name="firstname" id="inputfirstname" placeholder="First Name" value="<?if(isset($user[0]['firstname'])) echo $user[0]['firstname']; ?>" required>
				                  </div>
				              </div>
				              <div class="form-group">
				                  <label for="inputlastname" class="col-lg-2 col-sm-2 control-label">Last Name</label>
				                  <div class="col-lg-6">
				                      <input type="text" class="form-control" name="lastname" id="inputlastname" placeholder="Last Name"value="<?if(isset($user[0]['lastname'])) echo $user[0]['lastname']; ?>"  required>
				                  </div>
				              </div>
				              <div class="form-group">
				                  <label for="role_title" class="col-lg-2 col-sm-2 control-label">Role Type</label>
				                  <div class="col-lg-6">
				                      <select class="form-control" name="role_title" required="">
				                      	<option value="">Please Select</option>
				                      	<option value="admin" <?if(isset($user[0]['role_title'])) if($user[0]['role_title'] == 'admin') echo 'selected=""'; ?> >Administrator</option>
				                      	<option value="ALRAZI" <?if(isset($user[0]['role_title'])) if($user[0]['role_title'] == 'ALRAZI') echo 'selected=""'; ?> >LAB</option>
										  <option value="CCH" <?if(isset($user[0]['role_title'])) if($user[0]['role_title'] == 'CCH') echo 'selected=""'; ?> >CCH STAFF</option>
										  <option value="MOBILE_VAN" <?if(isset($user[0]['role_title'])) if($user[0]['role_title'] == 'MOBILE_VAN') echo 'selected=""'; ?> >MOBILE VAN</option>
				                      </select>
				                  </div>
				              </div>
				              <? if($is_edit){
				              $disabled = 'readonly';
				              $label = '<label class="label label-info" id="newEmail" style="cursor: pointer;">Change Email Address</label>';
				              $required = "";
				              }else{
				              	$disabled = '';
				              	$required = "required";
				              	$label = "";
				              	} ?>
				              <div class="form-group">
				                  <label for="email" class="col-lg-2 col-sm-2 control-label">Email</label>
				                  <div class="col-lg-6">
				                      <input type="email" class="form-control" name="email" id="email" placeholder="Email" value="<?if(isset($user[0]['email'])) echo $user[0]['email']; ?>" <?=$disabled;?><?=$required;?> >
				                      <?= $label;?>
				                      <p class="text-center" style="margin: 0px;text-align: left;">
				                      	<span id="message"></span>
                                        </p>
				                  </div>
				              </div>
				              <div class="form-group" id="newEmailField" style="display: none;">
				                  <label for="email" class="col-lg-2 col-sm-2 control-label">New Email</label>
				                  <div class="col-lg-6">
				                      <input type="email" class="form-control" name="newemail" id="newemail" placeholder="Email" value="" >
				                      <p class="text-center" style="margin: 0px;text-align: left;">
				                      	<span id="newmessage"></span>
                                        </p>
				                  </div>
				              </div>
				              <?if($is_edit){?>
				              <div class="form-group">
				                  <label for="inputPassword1" class="col-lg-2 col-sm-2 control-label">New Password</label>
				                  <div class="col-lg-6">
				                      <input type="password" class="form-control fpassword" value="" name="fpassword" id="fpasword" placeholder="Password">
				                  </div>
				              </div>
				              <div class="form-group">
				                  <label for="inputPassword1" class="col-lg-2 col-sm-2 control-label">Confirm Password</label>
				                  <div class="col-lg-6">
				                      <input type="password" class="form-control cpassword" value="" id="cpassword" name="password" placeholder="Confirm Password">
				                      <span class="checkpass"></span>
				                  </div>
				              </div>
				              <?}else{?>
				              <div class="form-group">
				                  <label for="inputPassword1" class="col-lg-2 col-sm-2 control-label">Password</label>
				                  <div class="col-lg-6">
				                      <input type="password" class="form-control" value="" name="password" id="inputPassword1" placeholder="Password" required>
				                  </div>
				              </div>
				              <?}?>
				              <div class="form-group">
				                  <div class="col-lg-offset-2 col-lg-10">
				                      <button type="submit" class="btn btn-danger">
				                      <?if($is_edit){
				                      	echo "Update";
				                      }else{
				                      	echo "Register";
				                      }
				                      	?>
				                      </button>
				                  </div>
				              </div>
				          </form>
				      </div>
				  </section>
			  </div>
		  </div>
		</section>
	</aside>
</div>
<?= $footer?>