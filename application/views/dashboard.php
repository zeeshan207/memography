<?= $header?>

<div class="container">
  <div class="row">
	  <div class="col-lg-6 col-lg-offset-3">
	  	<section class="panel">
	      <header class="panel-heading">
	          Check Report
	      </header>
	      <div class="panel-body">
	          <form id="token-form" class="form-horizontal" role="form" action="<?=base_url().'patient/report';?>" method="post">
	              <div class="form-group">
	                  <label for="inputEmail1" class="col-lg-3 col-sm-3 control-label">Enter Token</label>
	                  <div class="col-lg-9">
	                      <input type="text" name="token" class="form-control" id="inputEmail1" placeholder="Enter a token here" required>
	                  </div>
	              </div>
	              <div class="form-group">
	                  <div class="col-lg-offset-9 col-lg-3">
	                      <button type="submit" class="btn btn-info">Submit</button>
	                  </div>
	              </div>
	          </form>
	      </div>
	    </section>
	  </div>
  </div>
</div>

<?= $footer?>

<script>

	$('#token-form').on('submit', function(){

		var token = $('input[name=token]').val();
		var path = $(this).attr('action') +'/'+ token.trim();
		$(this).attr('action', path);
		
	});

</script>