<?

class Main extends MY_Controller{


	public function index()
	{
		$data['header'] = $this->load->view('files/header', array(), true);
		$data['footer'] = $this->load->view('files/footer', array(), true);
		$this->load->view('admin/login.php', $data);
	}

	public function view_report($rf_id)
	{
		$admin_or_user = $this->session->userdata('is_admin');
		if($admin_or_user == ""){
			return redirect('main');
		}
		

		$preccheck = $this->admin_model->prev_report($rf_id);
		$prev = $preccheck[0]['rf_id'];

		if($preccheck == false){
			$prev = "";
		}

		$nextcheck = $this->admin_model->next_report($rf_id);
		$next = $nextcheck[0]['rf_id'];

		if($nextcheck == false){
			$next = "";
		}

		$result = $this->admin_model->find_report($rf_id);
		
		$report_form = [];

		foreach ($result as $value) {
			
			$report_form[$value['rf_id']]['date'] 			= $value['date'];
			$report_form[$value['rf_id']]['mr_no'] 			= $value['mr_no'];
			$report_form[$value['rf_id']]['name'] 			= $value['name'];
			$report_form[$value['rf_id']]['surname'] 		= $value['surname'];
			$report_form[$value['rf_id']]['age'] 			= $value['age'];
			$report_form[$value['rf_id']]['address'] 		= $value['address'];
			$report_form[$value['rf_id']]['f_number'] 		= $value['f_number'];
			$report_form[$value['rf_id']]['h_number'] 		= $value['h_number'];
			$report_form[$value['rf_id']]['status'] 		= $value['status'];
			$report_form[$value['rf_id']]['child'] 			= $value['child'];
			$report_form[$value['rf_id']]['height'] 		= $value['height'];
			$report_form[$value['rf_id']]['weight'] 		= $value['weight'];
			$report_form[$value['rf_id']]['bmi'] 			= $value['bmi'];
			$report_form[$value['rf_id']]['test'] 			= $value['test'];
			$report_form[$value['rf_id']]['lmp_date'] 		= $value['lmp_date'];
			$report_form[$value['rf_id']]['post_meno'] 		= $value['post_meno'];
			$report_form[$value['rf_id']]['pre_meno'] 		= $value['pre_meno'];
			$report_form[$value['rf_id']]['pregnancy'] 		= $value['pregnancy'];
			$report_form[$value['rf_id']]['sc_date'] 		= $value['sc_date'];
			$report_form[$value['rf_id']]['investi'] 		= $value['investi'];
			$report_form[$value['rf_id']]['district_name'] 	= $value['district_name'];
			$report_form[$value['rf_id']]['tehsil_name'] 	= $value['tehsil_name'];
			$report_form[$value['rf_id']]['doc_file'] 		= $value['doc_file'];
			$report_form[$value['rf_id']]['fd_details'][] 	= array(
																'density' => $value['density'],
																'focal_mass' => $value['focal_mass'],
																'cm_cal' => $value['cm_cal'],
																'lbm_cal' => $value['lbm_cal'],
																'a_dis' => $value['a_dis'],
																'asymmetry' => $value['asymmetry'],
																'quadrant' => $value['quadrant'],
																'birad' => $value['birad'],
																'technique' => $value['technique'],
																 'r_breast' => $value['r_breast'],
																	'l_breast' => $value['l_breast'],
																	'impression' => $value['impression']
			);

		}
		
		$focal_mass1 = explode("*", $report_form[$rf_id]['fd_details'][0]['focal_mass']);
		$report_form[$rf_id]['fd_details'][0]['focal_mass'] = $focal_mass1;

		$focal_mass2 = explode("*", $report_form[$rf_id]['fd_details'][1]['focal_mass']);
		$report_form[$rf_id]['fd_details'][1]['focal_mass'] = $focal_mass2;

		// $height = explode(",", $report_form[$rf_id]['height']);
		$images = $this->admin_model->find_img($rf_id);
		
		

		$data['images'] = $images;
		$report_form[$rf_id]['height'] = $report_form[$rf_id]['height'];
		$data['current'] = $result[0]['rf_id'];
		$data['prev']	= $prev;
		$data['next']   = $next;
		$data['report'] = $report_form;
		$data['header'] = $this->load->view('files/header', array(), true);
		$data['footer'] = $this->load->view('files/footer', array(), true);
		$this->load->view('admin/view_report.php', $data);
	}


public function report_view($rf_id)
	{
		$admin_or_user = $this->session->userdata('is_admin');
		if($admin_or_user == ""){
			return redirect('main');
		}
		

		$preccheck = $this->admin_model->prev_report($rf_id);
		$prev = $preccheck[0]['rf_id'];

		if($preccheck == false){
			$prev = "";
		}

		$nextcheck = $this->admin_model->next_report($rf_id);
		$next = $nextcheck[0]['rf_id'];

		if($nextcheck == false){
			$next = "";
		}

		$result = $this->admin_model->find_report($rf_id);

		// echo "<pre>";
		// print_r($result);
		// echo "</pre>";die;
		
		$report_form = [];

		foreach ($result as $value) {
			
			$report_form[$value['rf_id']]['date'] 			= $value['date'];
			$report_form[$value['rf_id']]['mr_no'] 			= $value['mr_no'];
			$report_form[$value['rf_id']]['name'] 			= $value['name'];
			$report_form[$value['rf_id']]['surname'] 		= $value['surname'];
			$report_form[$value['rf_id']]['age'] 			= $value['age'];
			$report_form[$value['rf_id']]['address'] 		= $value['address'];
			$report_form[$value['rf_id']]['f_number'] 		= $value['f_number'];
			$report_form[$value['rf_id']]['h_number'] 		= $value['h_number'];
			$report_form[$value['rf_id']]['status'] 		= $value['status'];
			$report_form[$value['rf_id']]['child'] 			= $value['child'];
			$report_form[$value['rf_id']]['height'] 		= $value['height'];
			$report_form[$value['rf_id']]['weight'] 		= $value['weight'];
			$report_form[$value['rf_id']]['bmi'] 			= $value['bmi'];
			$report_form[$value['rf_id']]['test'] 			= $value['test'];
			$report_form[$value['rf_id']]['lmp_date'] 		= $value['lmp_date'];
			$report_form[$value['rf_id']]['post_meno'] 		= $value['post_meno'];
			$report_form[$value['rf_id']]['pre_meno'] 		= $value['pre_meno'];
			$report_form[$value['rf_id']]['pregnancy'] 		= $value['pregnancy'];
			$report_form[$value['rf_id']]['sc_date'] 		= $value['sc_date'];
			$report_form[$value['rf_id']]['investi'] 		= $value['investi'];
			$report_form[$value['rf_id']]['district_name'] 	= $value['district_name'];
			$report_form[$value['rf_id']]['tehsil_name'] 	= $value['tehsil_name'];
			$report_form[$value['rf_id']]['doc_file'] 		= $value['doc_file'];
			$report_form[$value['rf_id']]['technique'] 		= $value['technique'];
			$report_form[$value['rf_id']]['impression'] 	= $value['impression'];
			$report_form[$value['rf_id']]['l_breast'] 		= $value['l_breast'];
			$report_form[$value['rf_id']]['r_breast'] 		= $value['r_breast'];
			
			$report_form[$value['rf_id']]['fd_details'][] 	= array(
																'density' 	=> $value['density'],
																'focal_mass'=> $value['focal_mass'],
																'cm_cal' 	=> $value['cm_cal'],
																'lbm_cal' 	=> $value['lbm_cal'],
																'a_dis' 	=> $value['a_dis'],
																'asymmetry' => $value['asymmetry'],
																'quadrant' 	=> $value['quadrant'],
																'birad' 	=> $value['birad'],
																// 'technique' => $value['technique'],
																// 'r_breast' 	=> $value['r_breast'],
																// 'l_breast' 	=> $value['l_breast'],
																// 'impression'=> $value['impression']
			);

		}
		
		$focal_mass1 = explode("*", $report_form[$rf_id]['fd_details'][0]['focal_mass']);
		$report_form[$rf_id]['fd_details'][0]['focal_mass'] = $focal_mass1;

		$focal_mass2 = explode("*", $report_form[$rf_id]['fd_details'][1]['focal_mass']);
		$report_form[$rf_id]['fd_details'][1]['focal_mass'] = $focal_mass2;

		// $height = explode(",", $report_form[$rf_id]['height']);
		$images = $this->admin_model->find_img($rf_id);
		
		

		$data['images'] = $images;
		$report_form[$rf_id]['height'] = $report_form[$rf_id]['height'];
		$data['current'] = $result[0]['rf_id'];
		$data['prev']	= $prev;
		$data['next']   = $next;
		$data['report'] = $report_form;
		$data['header'] = $this->load->view('files/header', array(), true);
		$data['footer'] = $this->load->view('files/footer', array(), true);
		$this->load->view('admin/report_view.php', $data);
	}

	private function section_parts($data)
	{
		$data['header'] = $this->load->view('files/header', array(), true);
		$data['head'] = $this->load->view('files/head', array(), true);
		$data['left_side'] = $this->load->view('files/left_side', array(), true);
		$data['footer'] = $this->load->view('files/footer', array(), true);

		return $data;
	}

	public function is_not_admin(){

		$is_admin = $this->session->userdata('is_admin');
		if( $is_admin == 0 )
			return redirect('main/all_reports');

	}

	public function admin_login()
	{

		$data = $this->input->post();
		
		$hashpassword = $data['password'];
        $hashed=md5($hashpassword);

		$email = $data['email'];
		$password = $hashed;

		$result = $this->admin_model->login_valid($email, $password);
        $is_admin = $result[0]['is_admin'];
		$firstname = $result[0]['firstname'];
		$role = $result[0]['role_title'];
        $user_id = $result[0]['user_id'];
       
		if( isset($result) ){

            $this->session->set_userdata('is_admin', $is_admin);
            $this->session->set_userdata('firstname', $firstname);
            $this->session->set_userdata('role', $role);
            $this->session->set_userdata('user_id',$user_id);
            $this->session->set_flashdata('login_success', 'Wellcome To Director');

            return redirect('main/all_reports');

        }else{

            $this->session->set_flashdata('login_failed', 'Invalid Email and Password');
            return redirect('main');

        }

	}

	public function admin_logout()
	{

		$this->session->sess_destroy();
        
        $this->session->set_flashdata('logout_success', 'Thanks..');
        return redirect('main');

	}

	public function add_report()
	{
		$this->is_not_admin();

		$data = array();
		$districts = $this->admin_model->get_districts();
		

		$data['districts'] = $districts;
		$data['is_edit'] = false;
		$data = $this->section_parts($data);
		$this->load->view('admin/add_report.php', $data);

	}

	public function get_tehsils($dis_id)
	{
		$tehsils = $this->admin_model->get_tehsils($dis_id);

		$opt = "";

        foreach ($tehsils as $tehsil) {
			
			$opt .= '<option value="'.$tehsil['tehsil_id'].'">';
			$opt .= ''.$tehsil['tehsil_name'].'';
			$opt .= '</option>';
        }

        if(!$tehsils){
            return false;
        }else{
            echo $opt;
        }
	}

	private function generate_random(){
        
        $seed = str_split('abcdefghijklmnopqrstuvwxyz'
                     .'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
                     .'0123456789'); // and any other characters
        shuffle($seed); // probably optional since array_is randomized; this may be redundant
        $rand = '';
        foreach (array_rand($seed, 8) as $k) $rand .= $seed[$k];
        
        return $rand;
    }

    public function submit_report()
	{

		$this->form_validation->set_rules('mr_no', 'MR No', 'required');
        $this->form_validation->set_rules('district', 'District', 'required');
        $this->form_validation->set_rules('tehsil', 'Tehsil', 'required');

        if ($this->form_validation->run() == FALSE)
		{
             redirect('main/latest_report');
		}
		else {

			$token = $this->generate_random();
			$token_check = $this->admin_model->find_token($token);

			if ($token_check) {
				$token = $this->generate_random();
			}

			$report = $this->input->post();
            $doc_file = $report['doc_file'];
			if ($doc_file == null) {
				$doc_file = "";
			}

			$query = Array(
				'token' 		=> $token,
				'mr_no' 		=> $report['mr_no'],
				'date' 			=> $report['date'],
				'name' 			=> $report['name'],
				'surname' 		=> $report['surname'],
				'age' 			=> $report['age'],
				'address' 		=> $report['address'],
				'tehsil_idFK' 	=> $report['tehsil'],
				'district_idFK' => $report['district'],
				'f_number' 		=> $report['fnumber'],
				'h_number' 		=> $report['hnumber'],
				'status' 		=> $report['status'],
				'child' 		=> $report['child'],
				'height' 		=> $report['height'],
				'weight' 		=> $report['weight'],
				'bmi' 			=> $report['bmi'],
				'test' 			=> $report['test'],
				'lmp_date' 		=> $report['lmp_date'],
				'post_meno' 	=> $report['post_meno'],
				'pre_meno' 		=> $report['pre_meno'],
				'pregnancy' 	=> $report['pregnancy'],
				'sc_date' 		=> $report['sc_date'],
				'investi' 		=> $report['investi'],
				'doc_file' 		=> $doc_file,
				'update_status' => '0',
                'user_id' 		=> $report['user_id']
			);


			$report_id = $this->admin_model->insert('report_form', $query);
			if (!$report_id) {

				$this->session->set_flashdata('report_error', 'Try Again');
			}

			// $ht = array(
			// 	'feet' => $report['feet'],
			// 	'inch' => $report['inch']
			// 	);
			// $height = implode(',', $ht);
			return redirect('main/latest_report');
		}
	}



    public function checkcheck()
    {
			$this->session->set_flashdata('report_success', 'comment Seccesfull. Thank You..');

			for($i = 0; $i < 2; $i++){

				$sf_mass = array(
		    		'sf_mass1' => $report['sf_mass1'][$i],
		    		'sf_mass2' => $report['sf_mass2'][$i]
		    		);
		    	$focalmass = implode('*', $sf_mass);

				$fd_details 	= array(
					'rf_idFK' 	=> $report_id,
					'density' 	=> $report['density'][$i],
					'focal_mass'=> $focalmass,
					'cm_cal' 	=> $report['cmcal'][$i],
					'lbm_cal' 	=> $report['lbmcal'][$i],
					'a_dis' 	=> $report['adis'][$i],
					'asymmetry' => $report['asymmetry'][$i],
					'quadrant' 	=> $report['quadrant'][$i],
					'birad' 	=> $report['birad'][$i],
					'technique' => $report['technique'],
					'r_breast' 	=> $report['r_breast'],
					'l_breast' 	=> $report['l_breast'],
					'impression'=> $report['impression']
				);

				$this->admin_model->insert('finding_details', $fd_details);
			}

			$this->session->set_flashdata('report_update_success', 'comment Seccesfull. Thank You..');

			for($i = 0; $i < 2; $i++){

				$sf_mass = array(
		    		'sf_mass1' => $report['sf_mass1'][$i],
		    		'sf_mass2' => $report['sf_mass2'][$i]
		    		);
		    	$focalmass = implode('*', $sf_mass);

				$fd_details = array(
					'density' 	=> $report['density'][$i],
					'focal_mass'=> $focalmass,
					'cm_cal' 	=> $report['cmcal'][$i],
					'lbm_cal' 	=> $report['lbmcal'][$i],
					'a_dis' 	=> $report['adis'][$i],
					'asymmetry' => $report['asymmetry'][$i],
					'quadrant' 	=> $report['quadrant'][$i],
					'birad' 	=> $report['birad'][$i],
					'technique' => $report['technique'],
					'r_breast' 	=> $report['r_breast'],
					'l_breast' 	=> $report['l_breast'],
					'impression'=> $report['impression']

				);

				$this->admin_model->update('finding_details', $fd_details, $report['fd_id'][$i], 'fd_id');
			}
	
    }


	public function edit_report($rf_id)
	{

		$report = $this->admin_model->find_report($rf_id);
		// $height = explode(",", $report[0]['height']);
		// $focal_mass = explode("*", $report[0]['focal_mass']);

		$data = array();
		$districts = $this->admin_model->get_districts();
		$images = $this->admin_model->find_img($rf_id);
		

		$report_form = [];

		foreach ($report as $value) {
			
			$report_form[$value['rf_id']]['date'] 			= $value['date'];
			$report_form[$value['rf_id']]['mr_no'] 			= $value['mr_no'];
			$report_form[$value['rf_id']]['name'] 			= $value['name'];
			$report_form[$value['rf_id']]['surname'] 		= $value['surname'];
			$report_form[$value['rf_id']]['age'] 			= $value['age'];
			$report_form[$value['rf_id']]['address'] 		= $value['address'];
			$report_form[$value['rf_id']]['f_number'] 		= $value['f_number'];
			$report_form[$value['rf_id']]['h_number'] 		= $value['h_number'];
			$report_form[$value['rf_id']]['status'] 		= $value['status'];
			$report_form[$value['rf_id']]['child'] 			= $value['child'];
			$report_form[$value['rf_id']]['height'] 		= $value['height'];
			$report_form[$value['rf_id']]['weight'] 		= $value['weight'];
			$report_form[$value['rf_id']]['bmi'] 			= $value['bmi'];
			$report_form[$value['rf_id']]['test'] 			= $value['test'];
			$report_form[$value['rf_id']]['lmp_date'] 		= $value['lmp_date'];
			$report_form[$value['rf_id']]['post_meno'] 		= $value['post_meno'];
			$report_form[$value['rf_id']]['pre_meno'] 		= $value['pre_meno'];
			$report_form[$value['rf_id']]['pregnancy'] 		= $value['pregnancy'];
			$report_form[$value['rf_id']]['sc_date'] 		= $value['sc_date'];
			$report_form[$value['rf_id']]['investi'] 		= $value['investi'];
			$report_form[$value['rf_id']]['district_name'] 	= $value['district_name'];
			$report_form[$value['rf_id']]['district_id'] 	= $value['district_id'];
			$report_form[$value['rf_id']]['tehsil_name'] 	= $value['tehsil_name'];
			$report_form[$value['rf_id']]['tehsil_id'] 		= $value['tehsil_id'];
			$report_form[$value['rf_id']]['doc_file'] 		= $value['doc_file'];
			$report_form[$value['rf_id']]['out_come'] 		= $value['out_come'];
			$report_form[$value['rf_id']]['fd_details'][] 	= array(
																'fd_id' => $value['fd_id'],
																'density' => $value['density'],
																'focal_mass' => $value['focal_mass'],
																'cm_cal' => $value['cm_cal'],
																'lbm_cal' => $value['lbm_cal'],
																'a_dis' => $value['a_dis'],
																'asymmetry' => $value['asymmetry'],
																'quadrant' => $value['quadrant'],
																'birad' => $value['birad'],
																'technique' => $value['technique'],
																'r_breast' => $value['r_breast'],
																'l_breast' => $value['l_breast'],
																'impression' => $value['impression']


			);

		}
		
		$focal_mass1 = explode("*", $report_form[$rf_id]['fd_details'][0]['focal_mass']);
		$report_form[$rf_id]['fd_details'][0]['focal_mass'] = $focal_mass1;

		$focal_mass2 = explode("*", $report_form[$rf_id]['fd_details'][1]['focal_mass']);
		$report_form[$rf_id]['fd_details'][1]['focal_mass'] = $focal_mass2;

		// $height = explode(",", $report_form[$rf_id]['height']);
		$images = $this->admin_model->find_img($rf_id);


		$data['images'] = $images;
		$data['height'] = $report_form[$rf_id]['height'];
		$data['districts'] = $districts;
		$data['report'] = $report_form;
		$data['is_edit'] = true;
		$data['id'] = $rf_id;
		
		$data = $this->section_parts($data);
		
		$this->load->view('admin/add_report',$data);

	}


	public function do_upload()
	{

		if(isset($_FILES['files'])){

			$file_name = $_FILES['files']['tmp_name'];
		
			$config = array();

			$config['upload_path'] 	= './assets/uploads/images/';
			$config['allowed_types']= 'gif|jpg|png';
			$config['max_size'] 	= '4096';
	        $config['max_width'] 	= '4096';
	        $config['max_height'] 	= '4096';

			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('files'))
			{	
				$error = array('error' => $this->upload->display_errors());
			}else{
				$data = array('upload_data' => $this->upload->data());
				
				$file_name = $data['upload_data']['file_name'];
			}
			$url = base_url()."assets/uploads/images/".''.$file_name;

			$div = "";

			$div .= '<div class="col-lg-3"  id="container">';
			$div .= '<input type="hidden" size="20"  name="imgs[]" value="'.$file_name.'">';
			$div .= '<button type="button" data-name="'.$file_name.'" style="color:red;" class="close" data-dismiss="modal"  id="img-close">×</button>';
			$div .= "<div class='profile-img' style='background-image: url(".$url."); '>";
			$div .= '</div>';
			$div .= '</div>';

			echo $div;

		}

		if(isset($_FILES['file'])){
			
			$config = array();

			$config['upload_path'] = './assets/uploads/files/';
			$config['allowed_types'] = 'doc|docx|zip';
			$config['max_size'] = '50000';

			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('file'))
			{	
				
				$error = array('error' => $this->upload->display_errors());	

			}
			else{
				$data = array('upload_data' => $this->upload->data());
				
				$file_name = $data['upload_data']['file_name'];
				
			}

			$div = "";

			$div .= '<div style="margin-left:157px;float:left;" class="date-blob">'.$file_name.'<span class="date-enable"><i style="margin-left:10px;cursor: pointer;" class="fa fa-times"></i></span>';
			$div .= '<input type="hidden" size="20"  name="doc_file" value="'.$file_name.'">';
			$div .= '</div>';
			echo $div;

		}

	}

	public function delete_file($file)
	{
		$path = 'assets/uploads/files/'.$file;

	    if(@unlink($path)){
	     	echo "success";
	    }else{
	     	echo 'fail';
	    }
	}


	public function delete_img($name)
    {

    	$result = $this->admin_model->delete_img($name);
    	
    	$path = 'assets/uploads/images/'.$name;

	    if(@unlink($path)){
	     	echo "success";
	    }else{
	     	echo 'fail';
	    }

    }

	public function update_report($rf_id)
	{
		// $this->is_not_admin();
		
		$report = $this->input->post();
		// echo "<pre>";
		// print_r($report); echo "</pre>";die;
		if($this->session->userdata('role') == 'CCH' || $this->session->userdata('role') == 'MOBILE_VAN' || $this->session->userdata('role') == 'admin')
		{

			foreach ($report['imgs'] as $img) {
			
				$img = array(
					'rf_idFK'	=> $rf_id,
					'image_name'=> $img
					);

				$this->admin_model->insert('image', $img);

			}

	    	$doc_file = $report['doc_file'];

	    	if($doc_file == null){
	    		$doc_file = "";
	    	}

	    	$query = Array(
	    		'date'			=> $report['date'],
	    		'mr_no'			=> $report['mr_no'],
	    		'name'			=> $report['name'],
	    		'surname'		=> $report['surname'],
	    		'age'			=> $report['age'],
	    		'address'		=> $report['address'],
	    		'tehsil_idFK'	=> $report['tehsil'],
	    		'district_idFK'	=> $report['district'],
	    		'f_number'		=> $report['fnumber'],
	    		'h_number'		=> $report['hnumber'],
	    		'status'		=> $report['status'],
	    		'child'			=> $report['child'],
	    		'height'		=> $report['height'],
	    		'weight'		=> $report['weight'],
	    		'bmi'			=> $report['bmi'],
	    		'test'			=> $report['test'],
	    		'post_meno'		=> $report['post_meno'],
	    		'pre_meno'		=> $report['pre_meno'],
	    		'pregnancy'		=> $report['pregnancy'],
	    		'sc_date'		=> $report['sc_date'],
	    		'lmp_date'		=> $report['lmp_date'],
	    		'sc_date'		=> $report['sc_date'],
	    		'investi'		=> $report['investi'],
	    		'doc_file'		=> $doc_file
	    		);
	    	
	    	if( !$this->admin_model->update('report_form', $query, $rf_id, 'rf_id') ){

				$this->session->set_flashdata('report_update_error', 'Try Again');
			}
		}
		else 
			if($this->session->userdata('role') == 'ALRAZI')
		{

			$query = Array(
				'update_status' => '1',
				'out_come' 		=> $report['out_come'],
				'technique' 	=> $report['technique'],
				'impression' 	=> $report['impression'],
				'l_breast' 		=> $report['l_breast'],
				'r_breast' 		=> $report['r_breast'],
	    		);

			$result = $this->admin_model->update('report_form', $query, $rf_id, 'rf_id'); 
			
			// if Lab updated
			$check_data = $this->admin_model->check_data($rf_id);
			
			if($check_data == true){

				$result = $this->admin_model->delete_old($rf_id);

			}

			for($i = 0; $i < 2; $i++){

				$sf_mass = array(
		    		'sf_mass1' => $report['sf_mass1'][$i],
		    		'sf_mass2' => $report['sf_mass2'][$i]
		    		);
		    	$focalmass = implode('*', $sf_mass);

				$fd_details = array(
					'rf_idFK' => $rf_id,
					'density' => $report['density'][$i],
					'focal_mass' => $focalmass,
					'cm_cal' => $report['cmcal'][$i],
					'lbm_cal' => $report['lbmcal'][$i],
					'a_dis' => $report['adis'][$i],
					'asymmetry' => $report['asymmetry'][$i],
					'quadrant' => $report['quadrant'][$i],
					'birad' => $report['birad'][$i],
					// 'technique' => $report['technique'],
					// 'r_breast' => $report['r_breast'],
					// 'l_breast' => $report['l_breast'],
					// 'impression' => $report['impression']
					);

				$this->admin_model->insert('finding_details', $fd_details);

				$fd_details = array(
					'technique' => $report['technique'],
					'r_breast' 	=> $report['r_breast'],
					'l_breast' 	=> $report['l_breast'],
					'impression'=> $report['impression']
					);

			}
		}

		return redirect('main/all_reports');

	}

	public function all_reports()
	{
		$admin_or_user = $this->session->userdata('is_admin');

		if($admin_or_user == "")
			return redirect('main');
		
		$result 	= $this->admin_model->all_reports();
		$districts 	= $this->admin_model->get_districts();

		$data = array();
		$data['reports'] = $result;
		$data['districts'] = $districts;
		$data = $this->section_parts($data);
		
		$this->load->view('admin/all_reports.php', $data);

	}

	public function latest_report()
	{

		$admin_or_user = $this->session->userdata('is_admin');
		if($admin_or_user == "")
			return redirect('main');

		$result = $this->admin_model->latest_reports();
		$districts = $this->admin_model->get_districts();

		$data = array();
		$data['reports'] = $result;
		$data['districts'] = $districts;
		$data = $this->section_parts($data);

		// echo "<pre>";
		// print_r($data); echo "</pre>";die;
	
		$this->load->view('admin/latest_reports.php', $data);

	}

	public function create_zip(){

	}

	public function deactive_report($rf_id)
	{
		$this->is_not_admin();
		

    	$query = Array(
    		'report_status'	=> '0'
    		);
    	
    	$this->admin_model->update('report_form', $query, $rf_id, 'rf_id');

		$this->session->set_flashdata('report_delete_success', 'comment Seccesfull. Thank You..');

		return redirect('main/all_reports');
	}

	public function create_user()
	{
		$this->is_not_admin();

		$data = array();
		
		$data['is_edit'] = false;
		$data = $this->section_parts($data);
		$this->load->view('admin/create_user.php', $data);

	}

	function check_email_availablity()
    {
        $email = $this->input->post('email');

        $get_result = $this->admin_model->email_exists($email);

        if(!$get_result){
            echo  json_encode(array('response' => true,'status' => 200));
        }else{
            echo json_encode(array('response' => false,'status' => 200));
        }
    }

	public function register()
	{
		$this->is_not_admin();

		$result = $this->input->post();

		$hashpassword = $result['password'];
        $hashed=md5($hashpassword);

        $data['password'] = $hashed;
		
		$query = array(
			'firstname'	=> $result['firstname'],
			'lastname'	=> $result['lastname'],
			'role_title'=> $result['role_title'],
			'email'		=> $result['email'],
			'password'	=> $hashed,
			'is_admin'	=> 0
			);

		if( !$this->admin_model->insert('user', $query) ){

			$this->session->set_flashdata('create_error', 'Try Again');
		}else{

			$this->session->set_flashdata('create_success', 'comment Seccesfull. Thank You..');
			
		}
		return redirect('main/create_user');
	}

	
	public function all_users()
	{

		$this->is_not_admin();

		$result = $this->admin_model->all_users();

		$data = array();
		$data['users'] = $result;
		$data = $this->section_parts($data);
		$this->load->view('admin/all_users.php', $data);

	}

	public function edit_user($user_id)
	{
		$this->is_not_admin();

		$user = $this->admin_model->find_user($user_id);
		$data = array();

		$data['user'] = $user;
		$data['is_edit'] = true;
		$data = $this->section_parts($data);
		$this->load->view('admin/create_user',$data);
	}

	public function update_user($user_id)
	{
		$this->is_not_admin();

		$result = $this->input->post();
		
		$hashpassword = $result['password'];
        $hashed=md5($hashpassword);

        $email = $result['newemail'];
        if($email == ""){
        	$email = $result['email'];
        }

		$query = array(
			'firstname' => $result['firstname'],
			'lastname' => $result['lastname'],
			'role_title' => $result['role_title'],
			'email' => $email,
			'password' => $hashed,
			);
		$user = $this->admin_model->update('user', $query, $user_id, 'user_id');

		if( !$user ){

			$this->session->set_flashdata('update_user_error', 'Try Again');
		}else{

			$this->session->set_flashdata('update_user_success', 'comment Seccesfull. Thank You..');
			
		}
		return redirect('main/all_users');
	}

	public function delete_user()
	{
		$this->is_not_admin();

		$result = $this->input->post();

		if( !$this->admin_model->delete_user($result['user_id']) ){

			$this->session->set_flashdata('user_delete_error', 'Try Again');
		}else{

			$this->session->set_flashdata('user_delete_success', 'Thank You..');
			
		}
		return redirect('main/all_users');

	}

	public function filter()
	{
		$result = $this->input->post();

		$query = [];

		foreach ($result as $index => $value) {

			if($value != ""){

				$query[$index] = $value;

			}

		}

		$get_result = $this->admin_model->filter_data($query);

		$body = "";
		
		if(empty($get_result)){

			$body .= '<tr><td colspan="4">No Records Found.</td></tr>';

		}else{

			$count = '0';
			foreach ($get_result as $value) {
				
				$body .= '<tr><td>'.(++$count).'</td><td>'.$value['token'].'</td>';
				$body .= '<td>'.$value['name'].'</td><td><div class="row"><div class="col-lg-3">';
				$body .= '<a href="'.base_url().'main/view_report/'.$value['rf_id'].'" class="btn btn-primary" target="_blank">View</a>';
				$body .= '</div></div></td></tr>';

			}

		}

		echo $body;

	}

}

?>