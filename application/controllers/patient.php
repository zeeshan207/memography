<?

class Patient extends MY_Controller{

	public function index()
	{


		$data['header'] = $this->load->view('files/header', array(), true);
		$data['footer'] = $this->load->view('files/footer', array(), true);
		$this->load->view('dashboard.php', $data);
	}

	public function report($id)
	{
		$token = $id;

		$result = $this->admin_model->token_report($token);
		
		if(!$result){
			redirect('patient');
		}


		$report_form = [];

		foreach ($result as $value) {
			
			$report_form[$value['rf_id']]['date'] 			= $value['date'];
			$report_form[$value['rf_id']]['mr_no'] 			= $value['mr_no'];
			$report_form[$value['rf_id']]['name'] 			= $value['name'];
			$report_form[$value['rf_id']]['surname'] 		= $value['surname'];
			$report_form[$value['rf_id']]['age'] 			= $value['age'];
			$report_form[$value['rf_id']]['address'] 		= $value['address'];
			$report_form[$value['rf_id']]['f_number'] 		= $value['f_number'];
			$report_form[$value['rf_id']]['h_number'] 		= $value['h_number'];
			$report_form[$value['rf_id']]['status'] 		= $value['status'];
			$report_form[$value['rf_id']]['child'] 			= $value['child'];
			$report_form[$value['rf_id']]['height'] 		= $value['height'];
			$report_form[$value['rf_id']]['weight'] 		= $value['weight'];
			$report_form[$value['rf_id']]['bmi'] 			= $value['bmi'];
			$report_form[$value['rf_id']]['test'] 			= $value['test'];
			$report_form[$value['rf_id']]['lmp_date'] 		= $value['lmp_date'];
			$report_form[$value['rf_id']]['post_meno'] 		= $value['post_meno'];
			$report_form[$value['rf_id']]['pre_meno'] 		= $value['pre_meno'];
			$report_form[$value['rf_id']]['pregnancy'] 		= $value['pregnancy'];
			$report_form[$value['rf_id']]['sc_date'] 		= $value['sc_date'];
			$report_form[$value['rf_id']]['investi'] 		= $value['investi'];
			$report_form[$value['rf_id']]['district_name'] 	= $value['district_name'];
			$report_form[$value['rf_id']]['tehsil_name'] 	= $value['tehsil_name'];
			$report_form[$value['rf_id']]['doc_file'] 		= $value['doc_file'];
			$report_form[$value['rf_id']]['fd_details'][] 	= array(
																'density' => $value['density'],
																'focal_mass' => $value['focal_mass'],
																'cm_cal' => $value['cm_cal'],
																'lbm_cal' => $value['lbm_cal'],
																'a_dis' => $value['a_dis'],
																'asymmetry' => $value['asymmetry'],
																'quadrant' => $value['quadrant'],
																'birad' => $value['birad']
																);

		}

		$rf_id = $result[0]['rf_id'];

		$focal_mass1 = explode("*", $report_form[$rf_id]['fd_details'][0]['focal_mass']);
		$report_form[$rf_id]['fd_details'][0]['focal_mass'] = $focal_mass1;

		$focal_mass2 = explode("*", $report_form[$rf_id]['fd_details'][1]['focal_mass']);
		$report_form[$rf_id]['fd_details'][1]['focal_mass'] = $focal_mass2;

		// $height = explode(",", $report_form[$rf_id]['height']);


		$images = $this->admin_model->find_img($rf_id);

		$data['images'] = $images;
		$report_form[$rf_id]['height'] = $report_form[$rf_id]['height'];
		$data['current'] = $result[0]['rf_id'];
		$data['report'] = $report_form;
		$data['header'] = $this->load->view('files/header', array(), true);
		$data['footer'] = $this->load->view('files/footer', array(), true);

		$this->load->view('admin/view_report.php', $data);

	}

}

?>