<?


class Admin_model extends CI_Model{

	public function __construct()
	{

		// Call the Model constructor
		parent::__construct();


	}

	public function login_valid($email, $password)
	{
		$this->db->where(['email'=>$email,'password'=>$password]);
		$q = $this->db->get('user');

		if( $q->num_rows() ){
			return $q->result_array();
		}else{
			return false;
		}
	}

	public function email_exists($email)
	{
		$this->db->where('email', $email);

		$query = $this->db->get('user');

		if( $query->num_rows() > 0 ){ 
			return true;
		} else { 
			return false;
		}
	}

	public function insert($table, $array)
    {
    	
		$result = $this->db->insert($table, $array);
        $qus_id = $this->db->insert_id();

        if(isset($result)){
            return  $qus_id;
        }else{
            return false;
        }
    }

    public function update($table, $array, $id, $pk){
        
        $this->db->where($pk, $id);
        $result = $this->db->update($table, $array); 
        
        if(isset($result)){
            return true;
        }else{
            return false;
        }
    }

    public function all_users()
    {   
        $query = $this->db->select('user_id, role_title, firstname, email')
        					->from('user')
        					->get();

        return $query->result_array();

    }

    public function find_user($user_id)
	{
		$q = $this->db->select(['user_id','firstname','role_title', 'lastname', 'email'])
						->where('user_id', $user_id)
						->get('user');

		return $q->result_array();
	}

	public function delete_user($user_id)
	{
		return $this->db->delete('user',['user_id'=>$user_id]);
	}

	public function all_reports()
    {

		$user_id=$this->session->userdata('user_id');
		$user_role = $this->db->select('role_title')
							  ->from('user')
							  ->where('user_id',$user_id)
							  ->get();

		$role = $user_role->result_array();				  


		if($role[0]['role_title'] == 'admin' || $role[0]['role_title'] == 'ALRAZI')					  
		{
			$query = $this->db->select('rf_id, name, mr_no, doc_file, user_id')
        					->from('report_form')
        					->where('update_status', '1')
        					->where('report_status', '1')
        					->order_by('rf_id', 'desc')
        					->get();
		}
		else
		{
			$query = $this->db->select('rf_id, name, mr_no, doc_file, user_id')
        					->from('report_form')
        					->where('update_status', '1')
        					->where('report_status', '1')
							->where('user_id',$user_id)
        					->order_by('rf_id', 'desc')
        					->get();
		}
        

        return $query->result_array();

    }

	public function latest_reports()
	{
		$user_id = $this->session->userdata('user_id');

		$user_role = $this->db->select('role_title')
							  ->from('user')
							  ->where('user_id',$user_id)
							  ->get();

		$role = $user_role->result_array();	

		if($role[0]['role_title'] == 'admin' || $role[0]['role_title'] == 'ALRAZI')					  
		{
			$query = $this->db->select('rf_id, name, mr_no, doc_file')
        					->from('report_form')
        					->where('update_status', '0')
        					->where('report_status', '1')
        					->order_by('rf_id', 'desc')
        					->get();
		}
		else
		{
			$query = $this->db->select('rf_id, name, mr_no, doc_file')
        					->from('report_form')
        					->where('update_status', '0')
        					->where('report_status', '1')
							->where('user_id',$user_id)
        					->order_by('rf_id', 'desc')
        					->get();
		}
        
		return $query->result_array();

	}


    public function filter_data($filter)
    { 

        $query = $this->db->select('rf_id, name, token')
        					->from('report_form')
        					->where($filter)
        					->get();

        return $query->result_array();

    }

    public function check_data($rf_id)
    {
    	$this->db->where('rf_idFk', $rf_id);

		$query = $this->db->get('finding_details');

		if( $query->num_rows() > 0 ){ 
			return true;
		} else { 
			return false;
		}
    }
    public function delete_old($rf_id)
	{
		return $this->db->delete('finding_details',['rf_idFk'=>$rf_id]);
	}

    public function find_report($rf_id)
	{

		// $query = $this->db->query("SELECT `report_form`.`rf_id`,`report_form`.`mr_no`,`report_form`.`post_meno`,`report_form`.`pre_meno`,`report_form`.`pregnancy`,`report_form`.`date`,`report_form`.`name`,`report_form`.`surname`,`report_form`.`age`,`report_form`.`address`,`report_form`.`f_number`,`report_form`.`h_number`,`report_form`.`status`,`report_form`.`child`,`report_form`.`height`,`report_form`.`weight`,`report_form`.`bmi`,`report_form`.`test`,`report_form`.`lmp_date`,`report_form`.`sc_date`,`report_form`.`investi`, `district`.`district_name`,`tehsils`.`tehsil_name`,`district`.`district_id`,`tehsils`.`tehsil_id`, `report_form`.`doc_file`, `report_form`.`out_come`,finding_details.*
		$query = $this->db->query("SELECT *
					from report_form
					LEFT JOIN (
								SELECT `district`.`district_id`, `district`.`district_name`
								FROM    `district`
								)`district` ON `report_form`.`district_idFK` = `district`.`district_id`
					LEFT JOIN (
								SELECT `tehsils`.`tehsil_id`, `tehsils`.`tehsil_name`
								FROM    `tehsils`
								)`tehsils` ON `report_form`.`tehsil_idFK` = `tehsils`.`tehsil_id`
					LEFT JOIN( 
								SELECT *
								FROM finding_details
									) finding_details ON report_form.rf_id = finding_details.rf_idFK
					WHERE	`report_form`.`rf_id` = ".$rf_id."");

		if( $query->num_rows() ){
			return $query->result_array();
		}else{
			return false;
		}

	}

	public function token_report($token)
	{
		$query = $this->db->query("SELECT `report_form`.`rf_id`,`report_form`.`mr_no`,`report_form`.`post_meno`,`report_form`.`pre_meno`,`report_form`.`pregnancy`,`report_form`.`date`,`report_form`.`name`,`report_form`.`surname`,`report_form`.`age`,`report_form`.`address`,`report_form`.`f_number`,`report_form`.`h_number`,`report_form`.`status`,`report_form`.`child`,`report_form`.`height`,`report_form`.`weight`,`report_form`.`bmi`,`report_form`.`test`,`report_form`.`lmp_date`,`report_form`.`sc_date`,`report_form`.`investi`, `district`.`district_name`,`tehsils`.`tehsil_name`,`district`.`district_id`,`tehsils`.`tehsil_id`, `report_form`.`doc_file`,finding_details.*
					from report_form
					LEFT JOIN (
								SELECT `district`.`district_id`, `district`.`district_name`
								FROM    `district`
								)`district` ON `report_form`.`district_idFK` = `district`.`district_id`
					LEFT JOIN (
								SELECT `tehsils`.`tehsil_id`, `tehsils`.`tehsil_name`
								FROM    `tehsils`
								)`tehsils` ON `report_form`.`tehsil_idFK` = `tehsils`.`tehsil_id`
					LEFT JOIN( 
								SELECT *
								FROM finding_details
									) finding_details ON report_form.rf_id = finding_details.rf_idFK
					WHERE	`report_form`.`mr_no` = '".$token."' AND `report_form`.`report_status` = '1'
								");

		if( $query->num_rows() ){
			return $query->result_array();
		}else{
			return false;
		}

	}

	public function find_img($rf_id)
	{
		
		$query = $this->db->query("SELECT `image`.`image_name`
								FROM `image`
								LEFT JOIN `report_form` ON `image`.`rf_idFK`=`report_form`.`rf_id`
								WHERE `image`.`rf_idFK` = ".$rf_id."");

		if( $query->num_rows() ){
			return $query->result_array();
		}else{
			return false;
		}

	}

	public function delete_img($image_name)
	{
		return $this->db->delete('image',['image_name'=>$image_name]);
	}

	public function next_report($rf_id)
	{
		$query = $this->db->query("SELECT *
								FROM (`report_form`)
								WHERE `rf_id` =  (select min(rf_id) from report_form where rf_id > ".$rf_id.")");

		if( $query->num_rows() ){
			return $query->result_array();
		}else{
			return false;
		}

	}

	public function prev_report($rf_id)
	{
		$query = $this->db->query("SELECT *
								FROM (`report_form`)
								WHERE `rf_id` =  (select max(rf_id) from report_form where rf_id < ".$rf_id.")");

		if( $query->num_rows() ){
			return $query->result_array();
		}else{
			return false;
		}

	}

	public function find_token($token)
	{
		$q = $this->db->select("*")
						->where('token', $token)
						->get('report_form');

		if( $q->num_rows() ){
			return $q->result_array();
		}else{
			return false;
		}

	}

	public function get_districts()
	{
		$q = $this->db->select("`district_id`, `district_name`")
						->order_by('district_name', 'asc')
						->get('district');

		if( $q->num_rows() ){
			return $q->result_array();
		}else{
			return false;
		}

	}

	public function get_tehsils($dis_id)
	{
		$q = $this->db->select("`tehsil_id`, `tehsil_name`")
						->order_by('tehsil_name', 'asc')
						->where('district_idFK', $dis_id)
						->get('tehsils');

		if( $q->num_rows() ){
			return $q->result_array();
		}else{
			return false;
		}

	}



}


?>